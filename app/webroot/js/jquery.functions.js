(function($) {
	$.extend({
		mysqlToDate : function(timestamp) {
			var now = new Date(timestamp);
			// Create an array with the current month, day and time
			var date = [now.getMonth() + 1, now.getDate(), now.getFullYear()];

			// Create an array with the current hour, minute and second
			var time = [now.getHours(), now.getMinutes(), now.getSeconds()];

			// Determine AM or PM suffix based on the hour
			var suffix = (time[0] < 12 ) ? "AM" : "PM";

			// Convert hour from military time
			time[0] = (time[0] < 12 ) ? time[0] : time[0] - 12;

			// If hour is 0, set it to 12
			time[0] = time[0] || 12;

			// If seconds and minutes are less than 10, add a zero
			for (var i = 1; i < 3; i++) {
				if (time[i] < 10) {
					time[i] = "0" + time[i];
				}
			}

			// Return the formatted string
			return date.join("/") + " " + time.join(":") + " " + suffix;
		},
		camelToSentace : function(str) {
			return str.replace(/^[a-z]|[A-Z]/g, function(v, i) {
				return i === 0 ? v.toUpperCase() : " " + v.toLowerCase();
			});
		},
		arrayToMap : function(key, value, array) {
			var response = {};
			$.each(array, function(i, item) {
				var model = $.first(item);
				response[model[key]] = model[value];
			});
			return response;
		},
		first : function(obj) {
			for (key in obj)
			return obj[key];
		},
		truncate : function(str, length) {
			return str.length > length ? str.substring(0, length - 3) + '...' : str;
		},
		firstKey : function(obj) {
			return $.first(obj);
		},
		singular : function(s) {
			if (s) {
				var last = s.slice(-1);
				if (last.toLowerCase() == 's') {
					s = s.substring(0, s.length - 1);
				}
			}
			return s;
		},
		formDialog : function(id, type, callback) {
			$.getJSON(BASE_FULL_URL + "js/form-config/" + type + "_form_config.json", function(formConfig) {
				function renderForm(obj) {
					var html = '<form class="pure-form dialog-form" onsubmit="return false;"><div id="featured_image_container"></div>';
					$.each(formConfig, function() {
						html += '<div class="pure-u-1 pure-u-md-1-3" >';
						html += '<label>';
						html += this.label.replace(/_/g, ' ').toUpperCase() + '</label>';
						html += $.template("form-field-" + this.type, {
							FormField : this
						});
						html += '</div><br/>';

					});
					html += "</form><br/><br/><br/><br/>";

					var alertId = jAlert(html, ( id ? 'EDIT ' : 'CREATE NEW ') + $.singular(type.replace("_", " ")).toUpperCase());

					$('.popup-ok').hide();
					$('.button-row').addClass('custom').append("<a href='javascript:void(0);' class='dialog-action-button pure-button pure-button-primary button-small' >SUBMIT</a> " + "<a href='javascript:void(0);' class='dialog-cancel-button pure-button button-small second' >CANCEL</a>");

					$('.dialog-action-button').click(function(e) {
						$('.dialog-form').ajaxFormSubmit(function(items) {
							$.ajaxProxy({
								url : BASE_FULL_URL + type + ( id ? ('/' + id) : '') + '.json',
								type : 'post',
								dataType : 'json',
								data : items,
								callback : function(data) {
									if(data.errorMessage != null){
										alert(data.errorMessage, "");
									}else{
										$('.dialog-cancel-button').trigger('click');
									
										if (callback) {
											callback(data);
										} else {
											window.location.reload();
										}
									}
								}
							});
						});
					});

					$('.dialog-cancel-button').click(function(e) {
						$('.popup-ok').trigger('click');
					});

					if (obj) {
						var data = $.first($.first(obj));
						for (var key in data) {
							$("*[name='" + key + "']:not([type='file'])").text(data[key]);
							$("*[name='" + key + "']:not([type='file'])").val(data[key]);
							$("*[name='" + key + "']:not([type='file'])").attr('data-select-value', data[key]);
							if (key == 'featured_image') {
								$('#featured_image_container').html("<img src='" + data[key] + "'  style='max-width:150px;' /><br/><br/>");
							}
						}
					}

					$('.ajax-select').ajaxSelect();
					$('.date-field').datetimepicker({
						mask : false,
						timepicker : false,
						format : 'Y-m-d'
					});
				}

				if (id) {
					$.ajax({
						type : 'GET',
						dataType : 'json',
						url : BASE_FULL_URL + type + "/" + id + ".json",
						data : {},
						success : function(d) {
							renderForm(d);
						}
					});
				} else {
					renderForm()
				}
			});
		},
		deleteObject : function(id, type, callback) {
			var alertId = jConfirm("Are you sure you want to delete this?", "", function(l) {
				if (l) {
					$.ajax({
						type : 'DELETE',
						dataType : 'json',
						url : BASE_FULL_URL + type + "/" + id + ".json",
						data : {},
						success : function(d) {
							$.alerts._hide(alertId);
							if (callback) {
								callback(d);
							} else {
								window.location.reload();
							}
						}
					});
				}
			});
		},
		sortable : function($el, update) {
			$el.nestedSortable({
				forcePlaceholderSize : true,
				handle : 'div',
				helper : 'clone',
				items : '> li',
				opacity : .6,
				placeholder : 'placeholder',
				revert : 250,
				axis : 'y',
				tabSize : 25,
				tolerance : 'pointer',
				toleranceElement : '> div',
				maxLevels : 3,
				isTree : true,
				expandOnHover : 700,
				startCollapsed : true,
				update : update
			});
		},
		humanize : function(str) {
			str = str || "";
			return str.replace(/_/g, ' ').replace(/-/g, ' ').toUpperCase();
		},
		logout : function(callback) {
			var alertId = jConfirm("Are you sure you want to sign out?", "", function(l) {
				if (l) {
					$.ajax({
						type : 'POST',
						dataType : 'json',
						url : BASE_FULL_URL + "users/logout",
						data : {},
						success : function(d) {
							$.alerts._hide(alertId);
							callback(d);
						}
					});
				}
			});
		},
		forgotPassword : function(callback) {
			var alertId = jAlert($.template('forgot-password', {}), "");
			$('input').first().focus();
			$('.popup_panel').hide();
			$('.reset-password-button').click(function() {
				$.ajax({
					type : 'POST',
					dataType : 'json',
					url : BASE_FULL_URL + "users/passwordReset",
					data : {
						email : $('#email').val()
					},
					success : function(d) {
						if (d.error) {
							noty({
								'text' : d.error.text,
								'timeout' : 5000
							});
						} else {
							$.alerts._hide(alertId);
							noty({
								'text' : "Thank you your password has been reset and email to your email address.",
								'timeout' : 5000
							});
						}
					}
				});
			});
			$('.forgot-password-close').click(function() {
				$.alerts._hide(alertId);
			});
			$('.popup_message form').enterBind(function() {
				$('.reset-password-button').trigger('click');
			});
		},
		login : function(callback) {
			var alertId = jAlert($.template('login', {}), "");
			$('input').first().focus();

			$('.popup_panel').hide();

			$('.forgot-password').click(function() {
				$.forgotPassword(callback);
			});

			$('.login-button').click(function() {
				$.ajax({
					type : 'POST',
					dataType : 'json',
					url : BASE_FULL_URL + "users/login",
					data : {
						username : $('#login_username').val(),
						password : $('#login_password').val()
					},
					success : function(d) {
						if (d.error) {
							noty({
								'text' : d.error,
								'timeout' : 5000
							});
						} else {
							$.alerts._hide(alertId);
							callback(d);
						}
					}
				});
			});
			$('.featherlight form').enterBind(function() {
				$('.login-button').trigger('click');
			});

			$('.popup-ok').parent().hide();
		},
		randomString : function() {
			var len = 32;
			var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
			var maxPos = $chars.length;
			var pwd = '';
			for ( i = 0; i < len; i++) {
				pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
			}
			return pwd;
		},
		template : function(name, data) {
			return new EJS({
				url : BASE_FULL_URL + "js/tpl/" + name + ".ejs"
			}).render(data);
		}
	});
})(jQuery);
