(function($) {
	$.fn.tooltipster = function(el) {
		return this.each(function(i, el) {
			var timeout = null;
			var delay = 1000;
			if ($(el).attr('title')) {
				$el = $(el).attr("data-tooltip", i);
				$el.attr('data-title', $el.attr('title')).removeAttr("title").hover(function() {
					$el = $(this);
					if(timeout != null) clearTimeout(timeout);
					timeout = setTimeout(function(){
						var linkPosition = $el.offset();
						var left = linkPosition.left - 30;
						var top = linkPosition.top;
						var $tooltip = $('<div class="tooltip" data-tooltip="' + i + '" style="left:' + left + 'px; top:' + top + 'px;">' + $el.attr('data-title') + '<div class="arrow"></div></div>').appendTo("body");
						var defaultPos = linkPosition.left - ($tooltip.width() / 2);
						if(defaultPos < 10){
							defaultPos = 10;
						}
						$tooltip.css({
							top : linkPosition.top - $tooltip.outerHeight() - 13,
							left : defaultPos
						});
						$tooltip.addClass("active");
					}, delay);
				}, function() {
					$el = $(this);
					$tooltip = $('div[data-tooltip=' + $el.data('tooltip') + ']').addClass("out");
					setTimeout(function() {
						$tooltip.removeClass("active").removeClass("out");
					}, 300);
					clearTimeout(timeout);
				});
			}
		});

	}
})(jQuery); 