(function($) {
	$.fn.extend({
		admin : function() {
			return this.each(function() {
				var $this = $(this);
				function init(callback) {
					$.ajax({
						type : 'GET',
						dataType : 'json',
						url : BASE_FULL_URL + "users/init",
						data : {},
						success : callback
					});
				}

				function buildContent(d) {
					var dataId = $.cookie('acive-tab');
					
					$this.html($.template('admin-layout', {user:d.user}));
					
					$('.vartical-tabs .tab').verticalTabs(function(id){
						$.cookie('acive-tab', id);
						
						switch(id){
							default:
								$("[data-model='" + id + "'].maintence-grid").maintenceGrid();
							break;
							case 'labels':
								$("[data-model='" + id + "']").labelsEditor($.arrayToMap('key', 'value', d.systemVariables)['app_labels']);
							break;
							case 'dashboard':
								$('.dashboard-table td').css('height', $(window).height() - 300);
								$('[data-action-type]').click(function(){
									switch($(this).attr('data-action-type')){
										case 'view-consumers':
											$('.tab[data-id="consumers"]').trigger('click');
										break;
										case 'app-labels':
											$('.tab[data-id="labels"]').trigger('click');
										break;
										case 'qr-code':
											jAlert("<div>Would you like to email or print this QR code?</div>", "");
											$('.popup_ok').val('CLOSE').after($.template('share-qr-code-dialog', {}));
											$('.popup_print').click(function() {
												$('.qrcode').printElement();
											});
											$('.popup_email').click(function() {
												$('.pop_up_email_address').toggle();
												$('.popup_send_email').unbind('click').click(function(){
													var val = $('.pop_up_email_value').val();
													var qrCode = $('.qrcode').html();
													$.ajaxProxy({
														url : BASE_FULL_URL + "users/sendEmail",
														type:'POST',
														data : {
															message : qrCode,
															send_to : val
														},
														type : 'POST',
														callback : function() {
															$('.popup_ok').trigger('click');
															jAlert("Email message sucessfully sent.", "");
														}
													});
												});
											});
										break;
									}
								});
								new QRCode($(".qrcode")[0], {text:"http://canikickitapp.com", width:190, height:190});	
								$('.submission_code').html($.arrayToMap('key', 'value', d.systemVariables)['website_submission_code']);
							break;
						}
						 bindSize();
						 $('.menu-btn').trigger('click');
					});
					
					$('.action-button').click(function() {
						switch($(this).attr('data-type')) {
							case 'logout':
								$.logout(function() {
									window.location.reload();
								});
								break;
						}
					});
					
					var toggled = false;
					$('.menu-btn').click(function(){
						$menu = $('.left_lane');
						if(!toggled){
							$menu.css('margin-left', 0);
							toggled = true;
						}else{
							$menu.css('margin-left', "-" + $('.left_lane').width() + "px");
							toggled = false;
						}
					});
					
					if(dataId == null){
						dataId = 'dashboard';
					}
					$('.tab[data-id="' + dataId + '"]').trigger('click');
					$('#footer').html($.arrayToMap('key', 'value', d.systemVariables)['copywrite']);
					
					$('.home-btn').click(function(){
						$('.tab[data-id="dashboard"]').trigger('click');
					});
				}

				init(function(d) {
					if (d.user)
						buildContent(d);
					else
						$.login(function() {
							init(buildContent);
						});
				});
			});
		}
	});
})(jQuery);

function bindSize() {
	var width = $(window).width();
	$('.vartical-tabs .right').css({
		'width': width//,
		//'min-height': $(window).height() - $('#header').height()
	});	
	$('#dashboard-chart').css('width', width);
	
	if(width  < 800){
		$('body').addClass('mobile-device');
		$('.dashboard-fullsize').hide();
		$('.dashboard-mobile').show();
	}else{
		$('body').removeClass('mobile-device');
		$('.dashboard-fullsize').show();
		$('.dashboard-mobile').hide();
	}
}
$(window).load(bindSize).resize(bindSize);
					