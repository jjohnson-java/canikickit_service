$(function() {

	$.ajaxSetup({
		cache : true
	});

	$(document).ajaxStart(function() {
		$.mask();
	});

	$(document).ajaxStop(function() {
		$.unmask();
	});

	$('#admin').admin();
	
	$('h5, a, div, span, button, img').tooltipster();
});
