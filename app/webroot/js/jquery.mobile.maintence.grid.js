(function($) {
	$.fn.extend({
		mobileMaintenanceGrid : function(loadData, showCallback) {
			return this.each(function() {
				var $this = $(this).html($.template('maintenance-grid', {}));
				var model = $(this).attr('data-id');
				var start = 0;
				var limit = 20;
				var sort = 'id';

				$('#grid-data thead tr').append($.template(model + "-maintenance-grid-header-row", {}));
				
				function renderCheckmark(val){
					return val == 1 ? '<img src="' + BASE_FULL_URL + 'img/checked.png" style="height:20px;" />' : '<img src="' + BASE_FULL_URL + 'img/unchecked.png" style="height:20px;opacity:0.0;" />';
				}
				
				function renderMoney(a){
					return ('$' + (a ? parseFloat(a).toFixed(2) : '0.00'));
				}
			
				var grid = $("#grid-data").bootgrid({
					ajax : true,
					post : function() {
						return {

						};
					},
					url : BASE_FULL_URL + model + ".json",
					formatters : {
						"regular_price" : function(column, row){
							return renderMoney(row.regular_price);
						},
						"wholesale_price" : function(column, row){
							return renderMoney(row.wholesale);
						},
						"total": function(column, row){
							return renderMoney(row.total);
						},
						"filename": function(column, row){
							return '<img src="' + BASE_FULL_URL + "files/products/" + row.filename + '" style="height:20px;" />';
						},
						"featured_image": function(column, row){
							return '<img src="' + BASE_FULL_URL + "files/products/" + row.featured_image + '" style="height:20px;" />';
						},
						"featured_image": function(column, row){
							return '<img src="' + BASE_FULL_URL + "files/products/" + row.featured_image + '" style="height:20px;" />';
						},
						"canceled" : function(column, row){
							return renderCheckmark(row.canceled);
						},
						"hold" : function(column, row){
							return renderCheckmark(row.hold);
						},
						"fullname" : function(column, row) {
							return row.first_name + " " + row.last_name;
						},
						"active" : function(column, row) {
							return renderCheckmark(row.active);
						},
						"customer" : function(column, row) {
							return row.user_id;
						},
						"amount_of_discount" : function(column, row) {
							return row.discount_type== 'percent' ? row.amount_of_discount + '%' : '$' . row.amount_of_discount;
						}
					}
				}).on("loaded.rs.jquery.bootgrid", function() {
					if($('.no-results').length == 0){
						$('#grid-data tbody tr').css('cursor', 'pointer').click(function() {
							$('tr.actions').remove();
							$('tr.highlight').removeClass('highlight');
							var productId = $(this).attr('data-row-id');
							if ($(this).attr('actions-on') == null) {
								$(this).after("<tr class='actions highlight'  ><td style='padding:0px;' colspan='" + $('#grid-data thead tr th').length + "' >" + $.template("" + model + "-maintenance-grid-action-column", {
									id : $(this).attr('id')
								}) + "</td></tr>");
	
								$(this).attr('actions-on', 'true');
								$(this).addClass('highlight');
								$(this).parent().find('.action-button').click(function(e) {
									
									e.preventDefault();
									e.stopPropagation();
									var dataId = productId;
	
									switch($(this).attr('data-type')) {
										case 'product_skus':
											$.productSkus(dataId);
											break;
										case 'finalize_order':
											$.finalizeOrderDialog(dataId);
											break;
										case 'print_coupon':
										case 'print_giftcard':
											$.printDialog($(this).attr('data-type'), dataId);
											break;
										case 'view_coupon_uses':
										case 'product_variations':
										case 'product_images':
										case 'product_inventory':
										case 'user_past_purchases':
											window.location.hash = $(this).attr('data-type');
											break;
										case 'return':
											$.formDialog(dataId, 'orders', function() {
												clearAndReload();
											});
											break;
										case 'create-new':
											$.formDialog(null, model, function() {
												clearAndReload();
											});
											break;
										case 'edit':
										case 'update':
											$.formDialog(dataId, model, function() {
												clearAndReload();
											});
											break;
										case 'delete':
											$.deleteObject(dataId, model, clearAndReload);
											break;
									}
								});
							} else {
								$(this).removeAttr('actions-on');
							}
						});
					};
					if(model == 'reports'){
						$('#grid-data-header, #grid-data-footer').hide();
						$('.select-cell').remove();
					}
				});
				
				function clearAndReload() {
					$("#grid-data").bootgrid('reload');
				}

				$('.actionBar').append('<a  class="pure-button" data-type="create-new" href="javascript:void(0);"  >CREATE NEW</a>&nbsp;&nbsp;<a  class="pure-button" data-type="remove-selected" href="javascript:void(0);"  >REMOVE SELECTED</a>');

				$this.find('[data-type="create-new"]').click(function() {
					$.formDialog(null, model, clearAndReload);
				});

				if (model == 'orders') {
					$this.find('[data-type="create-new"]').before('<a  class="pure-button wizard-button"  href="javascript:void(0);"  >Build Order</a>&nbsp;&nbsp;<a  class="pure-button price-check-button"  href="javascript:void(0);"  >Price Check</a>');
					$this.find('[data-type="create-new"]').remove();

					$('.price-check-button').click(function() {
						$.priceCheck();
					});

					$('.wizard-button').click(function() {
						$.inStoreCheckout();
					});
				}
				
				if(model == 'reports'){
					$('#grid-data-header, #grid-data-footer').hide();
					$('.select-cell').remove();
				}

				if (model == 'products') {
					$this.find('[data-type="create-new"]').after('<a  class="pure-button bulk-import-button"  href="javascript:void(0);"  >Import</a>&nbsp;&nbsp;<a  class="pure-button bulk-import-button"  href="javascript:void(0);"  >Export</a>');

					$('.bulk-import-button').click(function() {
						$.bulkImport();
					});
				}

				function setHeight() {
					$('.maintenance-grid').css('height', $(window).height() - $('.header').height() - $('.footer').height() - $('#search_container').height());
				}
				
				$('#grid-data-header .pure-button').each(function(i){
					if(i != 0){
						$(this).addClass('secondary');
					}
				});

				$(window).resize(setHeight);
				setHeight();

				if (showCallback) {
					showCallback();
					showCallback = null;
				}
			});
		}
	});
})(jQuery); 