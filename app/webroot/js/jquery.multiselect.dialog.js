(function($) {
	$.extend({
		multiSelectDialog : function(model, rowRenderer,  closeCallback, loadCallback, optionalId) {
			function __load(model) {
				$.ajaxProxy({
					url : BASE_FULL_URL +  model + ".json",
					data : {
						id : optionalId
					},
					dataType : 'json',
					type : 'get',
					callback : function(data) {
						var html = "<table style='width:300px;' class='multiselcttable'>";
						$.each(data[model], function(i, item){
							html += "<tr style='cursor:pointer;' data-value='" + $.first(item).id + "' ><td style='width:24px;background:#2F2F2F;vertical-align:middle;' ><img src='" + BASE_FULL_URL + "img/check.png' class='check-mark' style='width:24px;padding-top:5px;padding-left:5px;' /></td><td style='background:#2F2F2F;vertical-align:middle;text-align:left; padding:5px;'>" + rowRenderer(item) + "</td></tr>";
						})
						html += "</table>";
						var aid = jAlert(html, "");
						
						$('.popup_panel').html('<input type="button" class="multiselectdialog-ok" value="&nbsp;OK&nbsp;">');
						
						$('.multiselectdialog-ok').click(function(){
							var response = [];
							$('.multiselcttable tr[data-selected]').each(function(){
								response.push($(this).attr('data-value'));
							});
							closeCallback(response);
							$.alerts._hide(aid);
						});
						
						$('.multiselcttable tr').click(function(){
							var $tr = $(this);
							if($tr.attr('data-selected') == null){
								$tr.attr('data-selected', true).find('img').attr('src', BASE_FULL_URL + "img/uncheck.png");
							}else{
								$tr.attr('data-selected', null).find('img').attr('src', BASE_FULL_URL + "img/check.png");
							}
						});
						
						loadCallback();
					}
				});
			}
			__load(model);
		}
	});
})(jQuery);