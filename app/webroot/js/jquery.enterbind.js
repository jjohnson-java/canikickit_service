(function($) {
	$.fn.extend({
		enterBind : function(enterFunction) {
			$(this).unbind('keypress').keypress(function(event){
				if (event.which == 13 ) {
					if(event.target.nodeName.toLowerCase() != 'textarea'){
						event.preventDefault();
						enterFunction();
					}
				}
			});
		}
	});
})(jQuery);
