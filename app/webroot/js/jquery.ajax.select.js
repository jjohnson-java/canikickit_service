(function($) {
	$.fn.extend({
		ajaxSelect : function() {
			return this.each(function() {
				var $this = $(this);
				var dataName = $this.attr('data-name');
				$.ajaxProxy({
					url : BASE_FULL_URL + 'form/getSelectItems',
					type : 'get',
					dataType : 'json',
					data : {
						'data-name' : dataName
					},
					callback : function(data) {
						$.each(data, function(i, itemModel){
							for (var a in itemModel){
								var item = itemModel[a];
								$this.append("<option value='" + item.value + "'>" + item.label + "</option>");
							}
						});
					}
				});
			});

		}
	});
})(jQuery);
