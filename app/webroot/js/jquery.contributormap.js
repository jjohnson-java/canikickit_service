(function($) {
	$.fn.extend({
		contibutorMap : function(data, currentPosition, map) {
			
			return this.each(function() {
				var geocoder = new google.maps.Geocoder();
				var $this = $(this);
				$.each(data.contributors, function(i){
					var title = this.User.username.toUpperCase();
					var address = this.User.address + ' ' + this.User.city + ' ' + this.User.state + ' ' + this.User.zip;
					
					var hasSpecial = this.User.PurchasedSpecial.length > 0;
					geocoder.geocode({
						address : address
					}, function(locResult) {
						var lat = locResult[0].geometry.location.lat();
						var lng = locResult[0].geometry.location.lng();
						var latLng = new google.maps.LatLng(lat, lng);
						var marker = new google.maps.Marker({
					      position: latLng,
					      map: map,
					      animation: google.maps.Animation.DROP,
					      icon:hasSpecial ? 'img/kiki-visited.png' : 'img/kiki-king-logo.png'
					  	});
					  	google.maps.event.addListener(marker, 'click', function() {
						   console.log(this);
						});
					  	
					});
				});
				
				$.each(data.all, function(i, item){
					var title = this.name.toUpperCase();
					geocoder.geocode({
						address : item.vicinity
					}, function(locResult) {
						var lat = locResult[0].geometry.location.lat();
						var lng = locResult[0].geometry.location.lng();
						var latLng = new google.maps.LatLng(lat, lng);
						var marker = new google.maps.Marker({
					      position: latLng,
					      map: map,
					      animation: google.maps.Animation.DROP,
					      icon:'img/kiki-white.png'
					  	});
					  	google.maps.event.addListener(marker, 'click', function() {
						   
						});
					  	
					});
				});
				
				var lat = currentPosition.coords.latitude;
				var lng = currentPosition.coords.longitude;
				var latLng = new google.maps.LatLng(lat, lng);
				
				map.setCenter(latLng);
				
				var currentMarker = new google.maps.Marker({
			      position: latLng,
			      map: map,
			      icon:BASE_FULL_URL + 'timthumb.php?w=25&h=25&src=' + BASE_FULL_URL + 'featured_image/get/User/1'
			  	});
			});
		}
	});
})(jQuery);