(function($) {
	$.fn.extend({
		labelsEditor : function(json) {
			return this.each(function() {
				var $this = $(this);
				var data = $.parseJSON(json);
				
				for(var key in data){
					if(data[key].labels){
						data[key].title = key.toUpperCase().replace("_", " ");
						data[key].parentKey = key;
						$this.append($.template("label-editor-row", data[key]));
					}
				}
				
				$('.label-editor-row h3').click(function(){
					$(this).parent().find('.inner-labels').toggle();
				});
				
				$('.label-editor-row .save').click(function(){
					var data = {parentKey: $(this).attr('data-parent-key')};
					$(this).parent().find('textarea').each(function(){
						data[$(this).attr('name')] = $(this).val();
					});
					$.ajaxProxy({
						url : BASE_FULL_URL + 'system_variables/updateLabelSet',
						type : 'post',
						dataType : 'json',
						data : data,
						callback : function(data) {
							jAlert("Your labels have been saved. All devices have been updated.", "");
						}
					});
				});
			});
		}
	});
})(jQuery);

