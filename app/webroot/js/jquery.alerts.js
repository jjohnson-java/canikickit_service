var lastNoty = null;
(function($) {
	$.alerts = {};
	$.alerts._hide = function($noty) {
		$('.featherlight-close').trigger('click');
	}
	$.alerts._reposition = function() {
		$(window).trigger('resize');
	}
})(jQuery);

function jAlert(message, title, callback, optionButtonLabel) {
	if (title) {
		message = "<h1>" + title + "</h1>" + message;
	}
	
	message += renderButtonRow(optionButtonLabel);
	
	$.featherlight("<div>" + message + "</div>", {openSpeed:200, closeSpeed:200, closeOnClick:false});
	
	$('.popup-ok').click(function(){
		$('.featherlight-close').trigger('click');
	});
}

function renderButtonRow(optionButtonLabel){
	if(typeof optionButtonLabel == 'undefined'){
		optionButtonLabel = 'OK'
	}
	return "<div class='button-row'><a href='javascript:void(0);' class='pure-button button-small popup-ok' >" + optionButtonLabel + "</a></div>";
}

function renderButtonRowNoFormat(optionButtonLabel){
	if(typeof optionButtonLabel == 'undefined'){
		optionButtonLabel = 'OK'
	}
	return "<div style='text-align:center; margin-top:20px;'><a href='javascript:void(0);' class='pure-button button-small popup-ok' >" + optionButtonLabel + "</a></div>";
}

function jConfirm(message, title, callback, optionButtonLabel) {
	if (title) {
		message = "<h1>" + title + "</h1>" + message;
	}
	
	message += renderButtonRowNoFormat(optionButtonLabel);
	
	$.featherlight("<div>" + message + "</div>", {openSpeed:200, closeSpeed:200, closeOnClick:false});
	
	$('.popup-ok').click(function(){
		callback(true);
	});
	
	$('.featherlight-close').click(function(){
		
	});
};

function jPrompt(message, value, title, callback) {

};