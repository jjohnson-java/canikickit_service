(function($) {
	$.fn.extend({
		verticalTabs : function(callback) {
			return this.each(function(){
				$(this).click(function(){
					if($(this).attr('activated') == null){
						$(this).attr('activated', true);
						callback($(this).attr("data-id"));
					}
					
					var i = Number($(this).attr("id").substr(1));
		         	$(".tab").removeClass("blu");
		         	$(this).addClass("blu");
		         	$(".cnt").hide();
		         	$("#c"+i).show();
		         	
				});
		    });
		}
	});
})(jQuery);