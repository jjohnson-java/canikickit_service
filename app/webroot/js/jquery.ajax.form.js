(function($) {
	$.fn.extend({
		ajaxForm : function() {
			return this.each(function(){
				var $this = $(this);
				var id = $this.attr('data-id');
				var $submitBtn = $this.find('.submit-btn');
				var type = $this.attr('data-model-type');
	
				function __markInvalid(item, error) {
					var $item = $(item);
					$item.css('border', '1px dashed red');
					$item.parent().append("<div class='validation-error-container'>" + error + "</div>");
				}
	
				function __clearInvalid(items) {
					$('.validation-error-container').remove();
	
					$.each(items, function(i, item) {
						var $item = $(item);
						$item.css('border', '1px solid #CCCCCC');
					});
				}
	
				function __validate(items) {
					var hasErrors = false;
	
					__clearInvalid(items);
	
					$.each(items, function(i, $item) {
						var $this = $(this);
						if ($this.attr('data-validation') == '1') {
							var val = $this.val();
							if (val == '') {
								__markInvalid($item, "This field must have a value");
								hasErrors = true;
							}
						}
					});
					return !hasErrors;
				}
				$submitBtn.click(function() {
					var key = $this.attr('data-model-type');
					if (__validate($this.find('input, select, textarea'))) {
						function findByName(items, name){
							var item = null;
							$.each(items, function(){
								if(this.name == name){
									item = this;
								}
							});
							return item;
						}
						$this.ajaxFormSubmit(function(items){
							//get item adress; findByName(items, name)
							if(type == 'users'){
								var address = item.User.address + ' ' + item.User.city + ' ' + item.User.state + ' ' + item.User.zip;
								geocoder.geocode({
									address : address
								}, function(locResult) {
									var lat = locResult[0].geometry.location.lat();
									var lng = locResult[0].geometry.location.lng();
								  	google.maps.event.addListener(addMarker(lat, lng, 'img/kiki-visited.png'), 'click', function() {
									   $($('.contributor-container')[i]).trigger('click');
									});
									if(data.contributors.length != (index + 1)){
										processC(index + 1);
									}
								});
							}
							
							var proxy = window.Online ? $.ajaxProxy : $.localProxy;
							proxy({
								url : BASE_FULL_URL + type + ( id ? ('/' + id) : '') + '.json',
								type : 'post',
								dataType : 'json',
								data : items,
								objectType:type,
								callback : function(data) {
									if (data.success) {
										jAlert("Your save was successful!", "", function() {
											window.history.back(-1);
										});
									} else {
										jAlert("There was an error with saving. Please try again.", "");
									}
								}
							});
						});
					}
				});
	
				$this.attr('onsubmit', 'return:false;');
	
				if (id) {
					setTimeout(function() {
						
						$.ajaxProxy({
							url : BASE_FULL_URL + type + '/' + id + '.json',
							type : 'get',
							dataType : 'json',
							data : {},
							callback : function(data) {
								try{
									var obj = data[type][$.firstKey(data[type])];
									for (var i in obj) {
										$('input[name="' + i + '"], select[name="' + i + '"], textarea[name="' + i + '"]').val(obj[i]);
									}
								}catch(e){}
								
								$this.find('a').actionFormHandler(data, type, id);
								
								var val = $('#subject').val();
								$('#subject').val("RE: " + val);
								
								val = $('#message').val();
								$('#message').val("\r\n\r\n" + val);
								
							}
						});
					}, 1000);
					
					var actionBtnHTML = $.template(BASE_FULL_URL + "js/templates/" + type + "-form-action-button-html.ejs", {});
					$this.find('fieldset').before(actionBtnHTML);
					
					$('#thumbnail').thumbnail(id);
					
					$.titles($this);
				}
			});
		}
	});
})(jQuery);
