(function($) {
	$.fn.extend({
		maintenceGrid : function(optionalId) {
			function processAdditions(urlKey, closeData, dataId, model) {
				$.ajaxProxy({
					url : BASE_FULL_URL + model + "/" + urlKey,
					dataType : 'json',
					type : 'post',
					data : {
						ids : JSON.stringify(closeData),
						dataId : dataId
					},
					callback : function(data) {
					}
				});
			}

			function applyAdditions(dataId, dataKey, model) {
				$.ajaxProxy({
					url : BASE_FULL_URL + model + "/" + dataId + ".json",
					dataType : 'json',
					type : 'get',
					callback : function(data) {
						$.each($.first(data)[dataKey], function(i, item) {
							$('tr[data-value="' + item.id + '"]').trigger('click');
						});
					}
				});
			}

			return this.each(function() {
				var $this = $(this).html($.template('maintenance-grid', {}));

				var model = $(this).attr('data-model');
				var start = 0;
				var limit = 20;
				var sort = 'id';

				$this.find('.grid-data thead tr').append($.template(model + "-maintenance-grid-header-row", {}));

				function renderCheckmark(val) {
					return val == 1 ? '<img src="' + BASE_FULL_URL + 'img/checked.png" style="height:20px;" />' : '<img src="' + BASE_FULL_URL + 'img/unchecked.png" style="height:20px;opacity:0.0;" />';
				}

				function renderMoney(a) {
					return ('$' + ( a ? parseFloat(a).toFixed(2) : '0.00'));
				}

				var grid = $this.find(".grid-data").bootgrid({
					ajax : true,
					post : function() {
						return {
							dataId : optionalId
						};
					},
					url : BASE_FULL_URL + model + ".json",
					formatters : {
						"phone" : function(column, row) {
							return formatLocal("US", row.phone);
						},
						"specials_actions" : function(column, row) {
							return '<a href="javascript:void(0);" class="pure-button button-secondary action-button button-small" data-special-id="' + row.id + '" data-type="update-special">EDIT</a>&nbsp;&nbsp;' + '<a href="javascript:void(0);" class="pure-button button-secondary action-button button-small delete-icon" data-id="' + row.id + '" data-type="delete">DELETE</a>';
						},
						"regular_price" : function(column, row) {
							return renderMoney(row.regular_price);
						},
						"wholesale_price" : function(column, row) {
							return renderMoney(row.wholesale);
						},
						"total" : function(column, row) {
							return renderMoney(row.total);
						},
						"filename" : function(column, row) {
							return '<div style="background-image:url(' + BASE_FULL_URL + "files/products/" + row.filename + ');" class="grid-thumbnail"></div>';
						},
						"featured_image" : function(column, row) {
							return '<div style="background-image:url(' + row.featured_image + ');" class="grid-thumbnail"></div>';
						},
						"canceled" : function(column, row) {
							return renderCheckmark(row.canceled);
						},
						"hold" : function(column, row) {
							return renderCheckmark(row.hold);
						},
						"fullname" : function(column, row) {
							return row.first_name + " " + row.last_name;
						},
						"active" : function(column, row) {
							return renderCheckmark(row.active);
						},
						"customer" : function(column, row) {
							return row.user_id;
						},
						"amount_of_discount" : function(column, row) {
							return row.discount_type == 'percent' ? row.amount_of_discount + '%' : '$'.row.amount_of_discount;
						},
						"sent_to" : function(column, row) {
							return row.sent_to == 'all_users' ? 'ALL KICK IT MEMEBRS' : row.sent_to.toUpperCase();
						},
					}
				}).on("loaded.rs.jquery.bootgrid", function(a) {
					if($this.attr('rendered') == null){
						if ($this.find('.no-results').length == 0 && model != 'specials') {
							$this.find('.grid-data tbody tr').css('cursor', 'pointer').click(function(pE) {
								var dataId = $(this).attr('data-row-id');
								if (dataId) {
									var $elm = $(this);
									if (!$(pE.target).hasClass('actions') && !$(pE.target).hasClass('action-button')) {
										if ($elm.attr('actions-on') == null) {
											$('[data-type="close"]').trigger('click');
											$elm.after("<tr class='actions highlight'  ><td style='padding:0px;' colspan='" + $this.find('.grid-data thead tr th').length + "' >LOADING...</td></tr>");
											$elm.attr('actions-on', 'true');
											$elm.addClass('highlight');
											$.ajaxProxy({
												url : BASE_FULL_URL + model + "/" + dataId + ".json",
												type : 'get',
												callback : function(data) {
													$elm.next().find('td').html($.template("" + model + "-maintenance-grid-action-column", {
														id : dataId,
														model : $.first(data)
													}));

													$elm.next().find('td').append('<br/><br/><a  class="action-button" data-type="close" href="javascript:void(0);" style="font-size:12px;" >CLOSE</a>');

													$elm.next().find('td').click(function(e) {
														if ($(e.target).hasClass('action-button')) {

															switch($(e.target).attr('data-type')) {
																case  'choose-winner':
																	var timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
																	
																	jAlert($.template('set-winner', {winner_timestamp:timestamp, dataId:dataId}), "");
																	
																	$('.popup_ok').val('CLOSE').after('&nbsp;&nbsp;<input type="button" class="popup_send" value="&nbsp;SAVE WINNER&nbsp;">');
																	
																	$('.select-winner-select').ajaxSelect();
																	$('.popup_send').click(function() {
																		$('.set-winner-form').ajaxFormSubmit(function(items){
																			$.ajaxProxy({
																				url : BASE_FULL_URL + model + "/" + dataId + ".json",
																				data : items,
																				dataType : 'json',
																				type : 'POST',
																				callback : function() {
																					$('.popup_ok').trigger('click');
																					jAlert("Winner selected sucessfully", "");
																					clearAndReload();
																				}
																			});
																		});
																		
																	});
																	$.ajax({
																		type : 'GET',
																		dataType : 'json',
																		url : BASE_FULL_URL + model + "/" + dataId + ".json",
																		data : {},
																		success : function(d) {
																			$('.set-winner-form *').each(function(){
																				if($(this).attr('type') == null || $(this).attr('type') != 'file'){
																					var name = $(this).attr('name');
																					if(name != null){
																						$(this).val(d.trip.Trip[name]);
																					}
																				}
																			});
																		}
																	});
																	
																break;
																case 'close':
																	$elm.removeAttr('actions-on');
																	$elm.next().remove();
																	$elm.removeClass('highlight');
																	break;
																case 'text':
																	jAlert("<form class='text-form pure-form'>MESSAGE: <br/><br/><textarea></textarea></form>", "");
																	$('.popup_ok').val('CLOSE').after('&nbsp;&nbsp;<input type="button" class="popup_send" value="&nbsp;SEND&nbsp;">');

																	$('.popup_send').click(function() {
																		$.ajaxProxy({
																			url : BASE_FULL_URL + "text_messages.json",
																			data : {
																				message : $('.text-form textarea').val(),
																				sent_to : dataId
																			},
																			type : 'POST',
																			callback : function() {
																				$('.popup_ok').trigger('click');
																				jAlert("Text message sucessfully sent.", "");
																			}
																		});
																	});
																	break;
																case 'email':
																	jAlert("<form class='text-form pure-form'>MESSAGE: <br/><br/><textarea></textarea></form>", "");
																	$('.popup_ok').val('CLOSE').after('&nbsp;&nbsp;<input type="button" class="popup_send" value="&nbsp;SEND&nbsp;">');

																	$('.popup_send').click(function() {
																		$.ajaxProxy({
																			url : BASE_FULL_URL + "users/sendEmail",
																			type:'POST',
																			data : {
																				message : $('.text-form textarea').val(),
																				send_to : dataId
																			},
																			type : 'POST',
																			callback : function() {
																				$('.popup_ok').trigger('click');
																				jAlert("Email message sucessfully sent.", "");
																			}
																		});
																	});
																	break;
																case 'menu-upload':
																	jAlert("<form enctype='multipart/form-data' class='menu-form'><input type='file' name='menu' accept='application/pdf' style='width:256px;' /></form>", "");
																	$('.popup_ok').val('CLOSE').after('&nbsp;&nbsp;<input type="button" class="popup_upload" value="&nbsp;UPLOAD&nbsp;">');

																	var files;
																	$('.menu-form input').on('change', function(event) {
																		files = event.target.files;
																	});

																	$('.popup_upload').click(function() {
																		var data = new FormData();
																		$.each(files, function(key, value) {
																			data.append(key, value);
																		});

																		$.ajaxProxy({
																			url : BASE_FULL_URL + model + "/" + dataId + ".json",
																			data : data,
																			processData : false,
																			contentType : false,
																			dataType : 'json',
																			type : 'POST',
																			callback : function() {
																				$('.popup_ok').trigger('click');
																				jAlert("Menu uploaded sucessfully", "");
																			}
																		});
																	});
																	break;
																case 'actions':
																	$(e.target).next().fadeToggle();
																	break;
																case 'qrcode':
																		jAlert("<div class='qrcode' style='width:256px;'></div>", "");
																		new QRCode($(".popup_message .qrcode")[0], {text:"http://canikickitapp.com", width:190, height:190});	

																		$('.popup_ok').after('&nbsp;&nbsp;<input type="button" class="popup_print" value="&nbsp;PRINT&nbsp;">');
																		$('.popup_print').click(function() {
																			$('.qrcode').printElement();
																		});
																	break;
																case 'clone':
																	$.ajaxProxy({
																		url : BASE_FULL_URL + model + ".json",
																		data : {},
																		dataType : 'json',
																		type : 'get',
																		callback : function(data) {
																			var obj = $.first(data);
																			obj.id = null;
																			$.ajaxProxy({
																				url : BASE_FULL_URL + model + ".json",
																				type : 'POST',
																				data : obj,
																				dataType : 'json',
																				callback : function(data) {
																					$.formDialog(dataId, model, function() {
																						clearAndReload();
																					});
																				}
																			});
																		}
																	});
																	break;
																case 'create-new':
																	$.formDialog(null, model, function() {
																		clearAndReload();
																	});
																	break;
																case 'update':
																	$.formDialog(dataId, model, function() {
																		clearAndReload();
																	});
																	break;
																case 'manage-rewards':
																	$.multiSelectDialog('rewards', function(obj) {
																		return obj.Reward.name + '<br/><span style="font-size:13px;color:#222;">' + obj.Reward.terms + '</span>';
																	}, function(closeData) {
																		processAdditions('addRewardsToPromo', closeData, dataId, model);
																	}, function() {
																		applyAdditions(dataId, 'Reward', model);
																	}, dataId);
																	break;
																case 'manage-rewards-challanges':
																	$.multiSelectDialog('rewards', function(obj) {
																		return obj.Reward.name + '<br/><span style="font-size:13px;color:#222;">' + obj.Reward.terms + '</span>';
																	}, function(closeData) {
																		processAdditions('addRewardsToChallange', closeData, dataId, model);
																	}, function() {
																		applyAdditions(dataId, 'Reward', model);
																	}, dataId);
																	break;
																case 'manage-contributors':
																	$.multiSelectDialog('contributors', function(obj) {
																		return obj.User.username;
																	}, function(closeData) {
																		processAdditions('addContributorsToPromo', closeData, dataId, model);
																	}, function() {
																		applyAdditions(dataId, 'User', model);
																	}, dataId);
																	break;
																case 'create-new-special':
																	$.formDialog(null, 'specials', function() {
																		clearAndReload();
																	});

																	setTimeout(function(){
																		$('.dialog-form').append("<input type='hidden' value='" + dataId + "' name='user_id' />");
																	}, 1000);

																break;
																case 'update-special':
																	$.formDialog($(e.target).attr('data-special-id'), 'specials', function() {
																		clearAndReload();
																	});
																break;
																case 'delete':
																	jConfirm("Are you sure you want to do this?", "", function(d) {
																		if (d) {
																			$.ajaxProxy({
																				url : BASE_FULL_URL + model + "/" + dataId + ".json",
																				dataType : 'json',
																				type : 'delete',
																				callback : clearAndReload
																			});
																		}
																	});
																	break;
															}
														}
													});
													$elm.next().find('.specials-container').maintenceGrid(dataId);
													$elm.next().find('.sweepstakes-submissions').maintenceGrid(dataId);
													
													function handeChecked(id, checked){
														console.log(checked);
														if(checked){
															$('#' + id).attr('checked', 'checked');
														}else{
															$('#' + id).removeAttr('checked');
														}
													}
													
													if(data.user){
														handeChecked('validate_location_checkbox', data.user.User.validate_location == '1');
														handeChecked('showin_ui_checkbox', data.user.User.showin_ui == '1');
														handeChecked('kick_it_at_location', data.user.User.kick_it_at_location == '1');
													}

													$('#validate_location_checkbox').click(function(){
														$.ajaxProxy({
															url : BASE_FULL_URL + model + "/" + dataId + ".json",
															data : {id:dataId, validate_location:$(this).is(':checked') ? 1: 0},
															dataType : 'json',
															type : 'post'
														});
													});

													$('#showin_ui_checkbox').click(function(){
														$.ajaxProxy({
															url : BASE_FULL_URL + model + "/" + dataId + ".json",
															data : {id:dataId, showin_ui:$(this).is(':checked') ? 1: 0},
															dataType : 'json',
															type : 'post'
														});
													});
													
													$('#kick_it_at_location').click(function(){
														$.ajaxProxy({
															url : BASE_FULL_URL + model + "/" + dataId + ".json",
															data : {id:dataId, kick_it_at_location:$(this).is(':checked') ? 1: 0},
															dataType : 'json',
															type : 'post'
														});
													});
												}
											});
										}
									}
								}
							});
						};
						$this.attr('rendered', true);
						$('.model-title').html($('.tab.blu').text());
					}
				});

				function clearAndReload() {
					$this.attr('rendered', null);
					console.log('reload');
					$this.find(".grid-data").bootgrid('reload');
				}

				$('.actions.btn-group').hide();


				$this.find('.actionBar').append('<a  class="pure-button" data-type="create-new" href="javascript:void(0);" style="font-size:12px;" >CREATE NEW</a><div class="model-title"></div>');
				$this.find('.actionBar [data-type="create-new"]').after('<a  class="pure-button" data-type="export" href="javascript:void(0);" style="font-size:12px; background:#666 !important;" >EXPORT TO CSV</a>');

				$this.find('[data-type]').click(function() {
					switch($(this).attr('data-type')){
						case 'create-new':
							$.formDialog(null, model, clearAndReload);
						break;
						case 'export':
							$.ajaxProxy({
								url : BASE_FULL_URL + model + "/export",
								data : {},
								dataType : 'json',
								type : 'get',
								callback:function(d){
									window.location = BASE_FULL_URL + 'files/exports/' + d.filename;
								}
							});
						break;
					}
				});
			});
		}
	});
})(jQuery);
