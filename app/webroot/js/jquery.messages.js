(function($) {
	$.fn.extend({
		messages : function() {
			return this.each(function() {
				var $this = $(this);
				function reRender() {
					$.ajax({
						type : 'GET',
						dataType : 'json',
						url : BASE_FULL_URL + "received_messages.json",
						data : {},
						success : function(d) {
							if (d.receivedMessages.length > 0) {
								var id = $.randomString();
								var html = '<table class="pure-table" style="width:100%; border:none;">';
								html += '<thead>';
								html += '<tr>';
								html += '<th>CREATED ON</th>';
								eval("var firstMessage = " + d.receivedMessages[0].ReceivedMessage.message);
								for (var key in firstMessage) {
									html += '<th>' + $.humanize(key) + '</th>';
								}

								html += '<th></th>';
								html += '</tr>';
								html += '</thead><tbody>';
								$.each(d.receivedMessages, function(i, model) {
									eval("var message = " + model.ReceivedMessage.message);
									if (i % 2 == 0) {
										html += '<tr class="pure-table-odd">';
									} else {
										html += '<tr >';
									}
									html += '<td>';
									html += model.ReceivedMessage.create_timestamp;
									html += '</td>';
									for (var key in message) {
										html += '<td>';
										html += $.truncate(message[key], 30);
										html += '</td>';
									}
									html += '<td>';
									html += '<a href="javascript:void(0);" class="delete-action-button pure-button pure-button-primary button-small" data-id="' + message.id + '"  >Delete</a>';
									html += '</td>';
									html += ' </tr>';
								});
								html += '</tbody></table>';
								$this.html(html);

								$('.delete-action-button').click(function() {
									$.deleteObject($(this).attr('data-id'), "messages");
								});

							} else {
								$this.html('<div style="padding:20px;">There are currently no messages.</div>');
							}
						}
					});
				}

				reRender();
				setTimeout(reRender, 30000);
			});
		}
	});
})(jQuery);
