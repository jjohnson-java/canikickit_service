window.Online = true;
(function($) {
	$.extend({
		ajaxProxy : function(options) {
			function getFromLocalStorage(){
				var ajaxMap = $.totalStorage("AjaxMap");
				if(ajaxMap == null){
					ajaxMap = {};
				}
				var lastData = ajaxMap[getOfflineKey()];
				if(lastData == null){
					lastData = ajaxMap[getOfflineKey()];
				}
				options.callback(lastData ? lastData : {});
			}
			function getOfflineKey(){
				return options.type + options.url + JSON.stringify(options.data ? options.data : {});
			}
			if(window.Online){
				if (options.errorback)
					options.error = options.errorback;
				else {
					options.error = function(data) {
						try {
							data = $.parseJSON(data.responseText);
							
							noty({
								'text' : data.error.text ? data.error.text : "There was an unexpected error! Please try again.",
								'timeout' : 5000
							});
						} catch(e) {
							$.unmask();
							if (options.callback){
								getFromLocalStorage();
							}
						}
					}
				}
				if ($('.mask').length == 0) {
					
				}
				options.timeout = window.RequestTimeout;
				//options.data.username = $.cookie("username");
				//options.data.password = $.cookie("password");
				Pace.track(function(){
					$.ajax(options).done(function(data) {
						if (data && data.error) {
							
							noty({
								'text' : data.error.text ? data.error.text : "There was an unexpected error! Please try again.",
								'timeout' : 5000
							});
							if (options.errorback) {
								options.errorback(data);
							}
						} else if (options.callback) {
							var ajaxMap = $.totalStorage("AjaxMap");
							if(ajaxMap == null){
								ajaxMap = {};
							}
							ajaxMap[getOfflineKey()] = data;
							//$.totalStorage("AjaxMap", ajaxMap);
							options.callback(data)
						}
					});
				});
			}else if (options.callback) {
				if(options.type == 'get'){
					getFromLocalStorage();
				}else{
					jConfirm("You do not have an internet connection. Would you like to save this item locally?", "", function(doIt){
						if(doIt){
							getFromLocalStorage();
						}
					});
				}
				
			}
		}
	});
})(jQuery);
