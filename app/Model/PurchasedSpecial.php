<?
App::uses('AppModel', 'Model');

class PurchasedSpecial extends AppModel {
	public $virtualFields = array(
    	'user' => 'SELECT username FROM users where id=PurchasedSpecial.user_id',
    	'contributor' => 'SELECT username FROM users where id=PurchasedSpecial.contributor_id',
    	'special' => 'SELECT title FROM specials where id=PurchasedSpecial.special_id'
	);
}
