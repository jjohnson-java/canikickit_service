<?
App::uses('AppModel', 'Model');

class SpecialsComplete extends AppModel {
	
	public function findExact($challangeId, $contributorId, $userId){
		return $this -> find('all', array('conditions' => 
			array(
				'ChallangesComplete.challange_id' => $challangeId,
				'ChallangesComplete.user_id' => $userId,
				'ChallangesComplete.contributor_user_id' => $contributorId
			)
		));
	}
	
}
