<?
App::uses('AppModel', 'Model');

class SystemVariable extends AppModel {
	public function geValueByKey($key){
		$obj = $this->findByKey($key);
		return $obj['SystemVariable']['value'];
	}
}
