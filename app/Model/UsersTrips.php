<?
App::uses('AppModel', 'Model');

class UsersTrips extends AppModel {
	public $belongsTo = array('User', 'Trip');
}
