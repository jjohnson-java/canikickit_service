<?
App::uses('AppModel', 'Model');

class Sweepstake extends AppModel {
	public function getCurrent(){
		$startDate = date('Y-m-d');
		$conditions[' ? BETWEEN Sweepstake.start_timestamp and Sweepstake.end_timestamp'] = array($startDate);	
		return $this -> find('first', array(
			'conditions' => $conditions
		));
	}
}
