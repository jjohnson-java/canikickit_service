<?php

App::uses('Model', 'Model');

class AppModel extends Model {
	public $imageModels = array('User', 'Users', 'Advertisment', 'Advertisments', 'Special', 'Specials', 'Trip', 'Trips');
	public function afterFind($results, $primary = false) {
		parent::afterFind($results, $primary);
		foreach ($results as $key => $val) {
			foreach ($this->imageModels as $item) {
				if (isset($val[$item]['featured_image'])) {
					$model = Inflector::singularize($this -> alias);
					$id = $results[$key][$item]['id'];
					$results[$key][$item]['featured_image'] = Router::url('/', true) . "featured_image/get/$model/$id";
				}
			}
		}
		return $results;
	}
	
	public function beforeSave($options = array()) {
	    if(isset($this -> data['User']['featured_image']) && strpos($this -> data['User']['featured_image'], 'http') !== false){
	    	$this -> data['User']['featured_image'] = null;
			unset($this -> data['User']['featured_image']);
		}
	    return true;
	}

}
