<?php
Router::redirect('/', array('controller' => 'pages', 'action' => 'display', 'admin'));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::mapResources(array('purchased_special', 'social_sharings', 'contributors', 'consumers', 'text_messages', 'sweepstakes', 'advertisments', 'users', 'form', 'trips', 'specials', 'promotions','faqs', 'system_variables', 'tips'));
Router::parseExtensions();
CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';
