<?php

App::uses('AppController', 'Controller');

class RestController extends AppController {

	public $layout = 'ajax';

	public function setAsJSON() {
		$this -> autoRender = false;
		if(isset($this->request->query['jsonp'])){
			$this -> response -> type('text');
		}else{
			$this -> response -> type('json');
		}
	}

	function exportCsv($data, $fileName = '', $maxExecutionSeconds = '-1', $delimiter = ',', $enclosure = '"') {
		$this->autoRender=false;
		$this -> response -> type('text/csv');
		$flatData = array();
		foreach($data as $numericKey => $row){
			$flatRow = array();
			$this->flattenArray($row, $flatRow);
			$flatData[$numericKey] = $flatRow;
		}

		$headerRow = $this->getKeysForHeaderRow($flatData);
		$flatData = $this->mapAllRowsToHeaderRow($headerRow, $flatData);

		ini_set('max_execution_time', $maxExecutionSeconds);

		if(empty($fileName)){
			$fileName = WWW_ROOT . "files/exports/export_".date("Y-m-d").".csv";
		}

		$csvFile = fopen($fileName, 'w');

		fputcsv($csvFile,$headerRow, $delimiter, $enclosure);

		foreach ($flatData as $key => $value) {
			if(strpos($key, 'featured_image') === false){
				fputcsv($csvFile, $value, $delimiter, $enclosure);
			}
		}
		fclose($csvFile);

		$this->jsonResponse(array('filename' => basename($fileName)));
	}

	public function flattenArray($array, &$flatArray, $parentKeys = ''){
		foreach($array as $key => $value){
			if(strpos($key, 'featured_image') === false){
				$chainedKey = ($parentKeys !== '')? $parentKeys.'.'.$key : $key;
				if(is_array($value)){
					if($parentKeys == ''){
						$this->flattenArray($value, $flatArray, $chainedKey);
					}
				} else {
					$flatArray[$chainedKey] = $value;
				}
			}
		}
	}

	public function getKeysForHeaderRow($data){
		$headerRow = array();
		foreach($data as $key => $value){
			foreach($value as $fieldName => $fieldValue){
				if(array_search($fieldName, $headerRow) === false){
					$headerRow[] = $fieldName;
				}
			}
		}

		return $headerRow;
	}

	public function mapAllRowsToHeaderRow($headerRow, $data){
		$newData = array();
		foreach($data as $intKey => $rowArray){
			foreach($headerRow as $headerKey => $columnName){
				if(!isset($rowArray[$columnName])){
					//$rowArray[$columnName] = '';
					$newData[$intKey][$columnName] = '';
				} else {
					$newData[$intKey][$columnName] = $rowArray[$columnName];
				}
			}
		}

		return $newData;
	}


	public function jsonResponse($obj){
		if(isset($this->request->query['jsonp'])){
			$this -> response -> body("jsopCallback(" . json_encode($obj) . ");");
		}else{
			$this -> response -> body(json_encode($obj));
		}

	}

	public function returnItems($model, $key, $group = null, $conditions = array()){
		$this->setAsJSON();

		if(!isset($this->request->query['searchPhrase'])){
			$this->request->query['searchPhrase'] = "";
		}

		if(!isset($this->request->query['rowCount'])){
			$this->request->query['rowCount'] = 100;
		}

		if(!isset($this->request->query['current'])){
			$this->request->query['current'] = 1;
		}

		$parameters = $this->request->query;


		$keyword = $parameters['searchPhrase'];
		$conditions["OR"] = array();
		if ($keyword) {
			$val = strtolower($keyword);
			$fields = array_keys($model->getColumnTypes());
			foreach($fields as $field){
				$conditions['OR'][get_class($model) . '.' . $field . ' like '] = '%' . $val . '%';
			}
		}
		$vars = array(
			'conditions' => $conditions,
			'limit' => $this->request->query['rowCount'],
			'offset' => ($this->request->query['current'] - 1) * $this->request->query['rowCount']
		);
		//print_r($vars);
		if(isset($parameters['sort'])){
			$sort = key($parameters['sort']);
			$sortDir = current($parameters['sort']);
			$vars['order'] = array(get_class($model) . '.' . $sort . ' ' . $sortDir);
		}
		$total = $model -> find('count', array('conditions' => $conditions));
		$return = $model -> find('all', $vars);
		$response = array();
		foreach($return as $item){
			$obj = current($item);
			if(isset($item['User']['username'])){
				$obj['username'] = $item['User']['username'];
			}

			if(isset($item['Product']) && isset($item['Product']['name'])){
				$obj['productname'] = $item['Product']['name'];
			}

			if(isset($item['ProductVariation']) && isset($item['ProductVariation']['value'])){
				$obj['variationname'] = $item['ProductVariation']['value'];
			}

			array_push($response, $obj);
		}
		$this->jsonResponse(array("current" => $this->request->query['current'], "rowCount" => $this->request->query['rowCount'], "rows" => $response, "total" => $total));
	}
}
