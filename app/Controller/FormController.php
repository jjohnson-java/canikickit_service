<?php

App::uses('RestController', 'Controller');

class FormController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('PhoneType', 'User');

	public function getSelectItems() {
		$this->setAsJSON();
		$dataName = $this -> request -> query['data-name'];
		$response = array();
		switch($dataName) {
			case 'phone_carrier_service';
				$response = $this -> PhoneType -> find('all');
			break;
			case 'ticket-price';
				$values = array(1, 2, 3, 4, 5, 6,7,8,9,10,15,20,30,40,50);
				foreach ($values as $value) {
					$item = array();
					$item['TicketPrice']['value'] = $value;
					$item['TicketPrice']['label'] = $value;
					array_push($response, $item);
				}
			break;
			case 'number_of_bars_needed';
				$values = array(1, 2, 3, 4, 5, 6,7,8,9,10,15,20,30,40,50);
				foreach ($values as $value) {
					$item = array();
					$item['TicketPrice']['value'] = $value;
					$item['TicketPrice']['label'] = $value;
					array_push($response, $item);
				}
			break;
			case 'consumers';
				$consumers = $this -> User -> find('all', array('conditions' => array('type' => 'consumer')));
				foreach ($consumers as $consumer) {
					$item = array();
					$item['User']['value'] = $consumer['User']['id'];
					$item['User']['label'] = $consumer['User']['username'];
					array_push($response, $item);
				}
			break;
		}
		$this -> jsonResponse($response);
	}

}
