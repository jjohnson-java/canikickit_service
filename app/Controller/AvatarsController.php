<?php

App::uses('RestController', 'Controller');

class AvatarsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Avatar');


	public function index() {
		$this -> returnItems($this -> Avatar, 'advatars', 'id');
	}

	public function export(){
		$this->exportCsv($this->Avatar->find('all'));
	}

	public function view($id) {
		$avatar = $this -> Avatar -> findById($id);
		$this -> set(array('promotion' => $avatar, '_serialize' => array('promotion')));
	}

	public function add() {
		if ($this -> Avatar -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}


	public function edit($id) {
		$this -> Avatar -> id = $id;
		if ($this -> Avatar -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Avatar -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
