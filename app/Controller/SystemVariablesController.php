<?php

App::uses('RestController', 'Controller');

class SystemVariablesController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('SystemVariable');

	public function index() {
		$this -> returnItems($this -> SystemVariable, 'system_variables');
	}

	public function updateLabelSet() {
		$this -> setAsJSON();
		$parentKey = $this -> request -> data['parentKey'];
		$systemVariable = $this -> SystemVariable -> findByKey('app_labels');
		$labels = json_decode($systemVariable['SystemVariable']['value'], true);
		foreach ($labels as $labelKey => $labelVal) {
			if ($labelKey == $parentKey) {
				$labels[$labelKey]['labels'] = $this -> request -> data;
				unset($labels[$labelKey]['labels']['parentKey']);
			}
		}
		$systemVariable['SystemVariable']['value'] = json_encode($labels);
		$this -> SystemVariable -> save($systemVariable['SystemVariable']);
	}

	public function view($id) {
		$systemVariable = $this -> SystemVariable -> findById($id);
		$this -> set(array('systemVariable' => $systemVariable, '_serialize' => array('systemVariable')));
	}

	public function export() {
		$this -> exportCsv($this -> SystemVariable -> find('all'));
	}

	public function add() {
		if ($this -> SystemVariable -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> SystemVariable -> id = $id;
		if ($this -> SystemVariable -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> SystemVariable -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
