<?php

App::uses('RestController', 'Controller');

class AdvertismentsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Advertisment');


	public function index() {
		$this -> returnItems($this -> Advertisment, 'advertisments', 'name');
	}

	public function export(){
		$this->exportCsv($this->Advertisment->find('all'));
	}

	public function view($id) {
		$advertisment = $this -> Advertisment -> findById($id);
		$this -> set(array('promotion' => $advertisment, '_serialize' => array('promotion')));
	}

	public function add() {
		if ($this -> Advertisment -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}


	public function edit($id) {
		$this -> Advertisment -> id = $id;
		if ($this -> Advertisment -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Advertisment -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
