<?php

App::uses('RestController', 'Controller');

class SweepstakesController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('User', 'Sweepstake');

	public function index() {
		$this -> returnItems($this -> Sweepstake, 'sweepstakes');
	}

	public function getCurrent() {
		$this -> setAsJSON();
		$this -> jsonResponse(array('sweepstake' => $this -> Sweepstake -> getCurrent()));
		//$log = $this->Sweepstake->getDataSource()->getLog(false, false);
		//∂print_r($log);
	}

	public function export(){
		$this->exportCsv($this->Sweepstake->find('all'));
	}

	public function submitExternal() {
		$this->setAsJSON();
		$user = $this->User->findByEmail($this -> request -> data['email']);
		if(!isset($user['User']['id'])){
			$this -> request -> data['username'] = $this -> request -> data['email'];
			$this -> request -> data['password'] = $this -> request -> data['email'];
			$user = $this->User->save($this -> request -> data);
		}
		$this -> request -> data['user_id'] = $user['User']['id'];
		$this -> request -> data['tickets'] = 'website_submit';
		$this -> request -> data['sweepstakes_id'] = $this -> request -> data['promocode'];
		$this->jsonResponse(array('success' => $this -> Sweepstake -> save($this -> request -> data)));
	}

	public function view($id) {
		$sweepstake = $this -> Sweepstake -> findById($id);
		$this -> set(array('special' => $sweepstake, '_serialize' => array('special')));
	}

	public function add() {
		if ($this -> Sweepstake -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> Sweepstake -> id = $id;
		if ($this -> Sweepstake -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Sweepstake -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
