<?php

App::uses('RestController', 'Controller');

class SocialSharingsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('SocialSharing');

	public function index() {
		$this -> returnItems($this -> SocialSharing, 'social_sharings');
	}

	public function view($id) {
		$this->setAsJSON();
		$social_sharing = $this -> SocialSharing -> findById($id);
		$this -> set(array('social_sharing' => $social_sharing, '_serialize' => array('social_sharing')));
		$this -> jsonResponse($social_sharing);
	}

	public function export(){
		$this->exportCsv($this->SocialSharing->find('all'));
	}

	public function add() {
		if ($this -> SocialSharing -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> SocialSharing -> id = $id;
		if ($this -> SocialSharing -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> SocialSharing -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
