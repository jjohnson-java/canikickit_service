<?php


App::uses('Controller', 'Controller');


class AppController extends Controller {
	public $helpers = array('Combinator.Combinator');

	public function isUserNameUnique($name){
		if($this->User->findByUsername($name)){
			return false;
		}else{
			return true;
		}
	}
	
	public function decode($value){
		$value = str_replace("data:image/jpeg;base64", "", $value);
		$value = str_replace("data:image/png;base64", "", $value);
		return base64_decode($value);
	}

	public function handleActive($data){
		if(isset($data['active'])){
			if($data['active'] == 'on'){
				$data['active'] = 1;
			}else if($data['active'] == 'off'){
				$data['active'] = 0;
			}else{
				$data['active'] = 1;
			}
		}else{
			$data['active'] = 0;
		}
		return $data;
	}

	public function getUser(){
		$user = CakeSession::read('user');
		if(isset($user['User']['id'])){
			$user = $this->User->findById($user['User']['id']);
		}
		return $user;
	}

}
