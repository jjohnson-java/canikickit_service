<?php

App::uses('RestController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class TextMessagesController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('AdvertisedMessage', 'TextMessage', 'QuedTextMessage', 'User');

	public function index() {
		$this -> returnItems($this -> TextMessage, 'text_messages', 'name');
	}

	public function view($id) {
		$notification = $this -> TextMessage -> findById($id);
		$this -> set(array('notification' => $notification, '_serialize' => array('notification')));
	}

	public function smsMessage($message, $phone, $carrier) {
		
		$message = wordwrap($message, 70);
		$phone = str_replace("(", "", $phone);
		$phone = str_replace(")", "", $phone);
		$phone = str_replace("-", "", $phone);
		$phone = str_replace("-", "", $phone);
		$phone = str_replace(" ", "", $phone);
		$to = $phone . '@' . $carrier;
		$Email = new CakeEmail();
		$Email -> from(array('app@canikickit.com' => 'CAN I KICK IT'));
		$Email -> to($to);
		$Email -> from();
		$Email -> send($message);
	}

	public function cycleQued() {
		$messages = $this -> QuedTextMessage -> find('all', array('limit' => 10, 'offset' => 0, 'conditions' => array('status' => 'pending')));
		if (sizeof($messages) > 0) {
			foreach ($messages as $message) {
				$message['QuedTextMessage']['status'] = 'sent';
				$this -> QuedTextMessage -> save($message['QuedTextMessage']);
				$user = $this->User->findByUsername($message['QuedTextMessage']['sent_to']);
				$this->smsMessage($message['QuedTextMessage']['message'], $user['User']['phone'], $user['User']['phone_carrier_service']);
			}
			$this -> cycleQued();
		}
	}

	public function add() {
		$message = $this -> TextMessage -> save($this -> request -> data);
		
		if ($message) {
			$message['TextMessage']['status'] = 'sent';
			
			$this -> TextMessage -> save($message['TextMessage']);
			switch($message['TextMessage']['sent_to']) {
				case 'all_users' :
					foreach($this->User->find('all') as $user){
						if($user['User']['phone'] && $user['User']['phone_carrier_service']){
							$message['TextMessage']['sent_to'] = $user['User']['username'];
							$message['TextMessage']['status'] = 'pending';
							$message['TextMessage']['id'] = null;
							$this->QuedTextMessage->save($message['TextMessage']);
						}
					}
					$this -> cycleQued();	
					break;
				default:
					$user = $this->User->findById($this->request->data['sent_to']);
					$message['TextMessage']['sent_to'] = $user['User']['username'];
					$this -> TextMessage -> save($message['TextMessage']);
					$this->smsMessage($message['TextMessage']['message'], $user['User']['phone'], $user['User']['phone_carrier_service']);
				break;
			}
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> TextMessage -> id = $id;
		if ($this -> TextMessage -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> TextMessage -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
