<?php
require_once dirname(__FILE__) . "/lib/simpletest/browser.php";
require_once dirname(__FILE__) . "/lib/simple_html_dom.php";

App::uses('RestController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Promotion', 'Trip', 'User', 'SystemVariable', 'SpecialsUsers', 'Sweepstake');

	public function index() {
		$this -> returnItems($this -> User, 'users');
	}

	public function export() {
		$this -> exportCsv($this -> User -> find('all'));
	}

	public function addSpecialsToUser() {
		$this -> setAsJSON();
		$user = $this -> User -> findById($this -> request -> data['dataId']);
		foreach (explode(",", $this->request->data['dataId']) as $sid) {
			$this -> SpecialsUsers -> save(array('special_id' => $sid, 'user_id' => $user['User']['id']));
		}
	}
	
	public function getCurrentLocation($userId){
		$this -> setAsJSON();
		$user = $this -> User -> findById($userId);
		
		//$lat = 39.636981060583;
		//$lng = -104.90441238389;
		
		$lat = $user['User']['latitude'];
		$lng = $user['User']['longitude'];
		
		$this->jsonResponse(array(
			"latitude" => $lat,
			"longitude" => $lng,
			"coords" => array(
				"latitude" => $lat,
				"longitude" => $lng
			)
		));
		
	}
	
	
	public function updateLocation($userId){
		
		if($this->request->data['location']){
			$data = json_decode($this->request->data['location'], true);
		}else{
			$data = json_decode(file_get_contents('php://input'), true);
		}
		
		$this -> setAsJSON();
		$user = $this -> User -> findById($userId);
		$user['User']['latitude'] = $data['location']['latitude'];
		$user['User']['longitude'] = $data['location']['longitude'];
		
		if(strpos($this -> request -> data['featured_image'], 'http') !== false){
			unset($this -> request -> data['featured_image']);
		}
		
		
		$this -> User -> save($user['User']);
	}

	public function enterWeeklyPromotion() {
		$this -> setAsJSON();
		$user = $this -> User -> findById($this -> request -> query['userId']);
		$response = array();
		$sweepstake = $this -> Sweepstake -> getCurrent();
		if ((intval($user['User']['number_of_tickets'])) == intval($sweepstake['Sweepstake']['ticket_price'])) {
			$response['success'] = false;	
			$response['message'] = 'error_min_tickets';
			$response['user'] = $user;
		} else if ((intval($user['User']['number_of_tickets']) - 1) < intval($sweepstake['Sweepstake']['ticket_price'])) {
			$response['success'] = false;	
			$response['message'] = 'error_not_enough_tickets';
			$response['user'] = $user;
		} else {
			$response['success'] = true;
			$this -> Promotion -> save(array('user_id' => $user['User']['id'], 'sweepstake_id' => $sweepstake['Sweepstake']['id'], 'tickets' => $sweepstake['Sweepstake']['ticket_price']));
			$user['User']['number_of_tickets'] = intval($user['User']['number_of_tickets']) - intval($sweepstake['Sweepstake']['ticket_price']);
			if(strpos($this -> request -> data['featured_image'], 'http') !== false){
				unset($this -> request -> data['featured_image']);
			}
			$this -> User -> save($user['User']);
			$response['user'] = $user;
		}
		$this -> jsonResponse($response);
	}

	public function findFriends() {
		$this -> setAsJSON();
		$user = $this -> getUser();
		$contacts = json_decode($this -> request -> data['contacts']);

		$response = array();
		$this -> jsonResponse($response);
	}

	public function view($id) {
		$user = $this -> User -> findById($id);
		$this -> set(array('user' => $user, '_serialize' => array('user')));
	}
	
	public function profileInfo() {
		$this -> setAsJSON();
		$user = $this -> getUser();
		$response = array();
		$response['allCurrentLeaders'] = $this -> User -> find('all', 
			array(
				'order' => array('User.number_of_tickets DESC'), 
				'limit' => 10,
				 'recursive' =>  0,
			));

		$response['trips'] = $this -> Trip -> find('all', array('order' => array('Trip.number_of_bars_needed')));
		$this -> jsonResponse($response);
	}

	public function profileData() {
		$this -> setAsJSON();
		$user = $this -> getUser();
		$response = array();
		$response['currentLeaders'] = $this -> User -> find('all', array('order' => array('User.number_of_tickets DESC'), 'limit' => 3));

		$response['allCurrentLeaders'] = $this -> User -> find('all', array('order' => array('User.number_of_tickets DESC'), 'limit' => 20));

		$response['trips'] = $this -> Trip -> find('all', array('order' => array('Trip.number_of_bars_needed')));
		$this -> jsonResponse($response);
	}

	public function profile() {
		$this -> setAsJSON();
		$user = $this -> getUser();

		$this -> jsonResponse($user);
	}

	public function isUserNameUnique($name) {
		$user = $this -> User -> findByUsername($name);
		return $user == null || sizeof($user) == 0;
	}

	public function isEmailUnique($name) {
		$user = $this -> User -> findByEmail($name);
		return $user == null || sizeof($user) == 0;
	}

	public function sendEmail() {
		$this -> setAsJSON();
		$user = $this -> User -> findById($this -> request -> data['send_to']);
		$Email = new CakeEmail();
		$Email -> template('default', 'default');
		$Email -> emailFormat('html');
		$Email -> to(isset($user['User']) ? $user['User']['email'] : $this -> request -> data['send_to']);
		$Email -> subject('Can I Kick it');
		$Email -> from('jjohnson.java@gmail.com');
		$Email -> viewVars(array('message' => $this -> request -> data['message']));
		$Email -> send();
	}

	public function add() {
		$this -> setAsJSON();
		if ($this -> isUserNameUnique($this -> request -> data['username']) && $this -> isEmailUnique($this -> request -> data['email'])) {
			$data = $this -> handleActive($this -> request -> data);
			$success = $this -> User -> save($data);
			if ($success) {
				$Email = new CakeEmail();
				$Email -> template('welcome', 'default');
				$Email -> emailFormat('html');
				$Email -> to($data['email']);
				$Email -> subject('Can I Kick it registration');
				$Email -> from('jjohnson.java@gmail.com');
				$Email -> viewVars(array('user' => $data));
				$Email -> send();
				$message = 'Saved';
			} else {
				$message = 'Error';
			}
		} else {
			$message = 'Username not unique';
			$success = false;
		}

		$this -> jsonResponse(array('success' => $success, 'message' => $message));
	}

	public function getMenu($userId) {
		$this -> autoRender = false;
		$this -> response -> type('application/pdf');
		$user = $this -> User -> findById($userId);
		if ($user['User']['menu']) {
			
			$this -> response -> body(base64_decode(file_get_contents(WWW_ROOT . 'files/' . $user['User']['menu'])));
		}
	}

	public function edit($id) {
		if (isset($this -> request -> form[0]['tmp_name'])) {
			$fileContents = base64_encode(file_get_contents($this -> request -> form[0]['tmp_name']));
			$filename = 'upload_bytes_' . uniqid();
			file_put_contents(WEB_ROOT . 'files/' . $filename, $fileContents);
			$this -> request -> data['menu'] = $filename;
		}

		$this -> setAsJSON();
		$this -> User -> id = $id;
		if(strpos($this -> request -> data['featured_image'], 'http') !== false){
			unset($this -> request -> data['featured_image']);
		}
		
		if ($this -> User -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> jsonResponse(array('message' => $message));
	}

	public function delete($id) {
		if ($this -> User -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function init() {

		$this -> setAsJSON();
		$response = array();

		$response['user'] = CakeSession::read('user');
		$response['systemVariables'] = $this -> SystemVariable -> find('all');
		$this -> jsonResponse($response);
	}

	public function login() {
		$this -> setAsJSON();
		$response = array();
		$error = "Invalid username or password!";
		$isSocialLogin = isset($this -> request -> data['socialType']) && $this -> request -> data['socialType'] != null && $this -> request -> data['socialType'] != "kickit";
		
		if (!$isSocialLogin) {
			$username = isset($this -> request -> query['username']) ? $this -> request -> query['username'] : $this -> request -> data['username'];
			$password = isset($this -> request -> query['password']) ? $this -> request -> query['password'] : $this -> request -> data['password'];
			$user = $this -> User -> find('first', array('conditions' => array('User.username' => $username, 'User.password' => $password)));
			if ($user) {
				CakeSession::write('user', $user);
				$response['user'] = $user;
				$response['systemVariables'] = $this -> SystemVariable -> find('all');
				$response['success'] = true;
				$this -> jsonResponse($response);
			} else {
				$this -> jsonResponse(array('success' => false, 'error' => $error));
			}
		} else {
			if (isset($this -> request -> data['featured_image'])) {
				$this -> request -> data['featured_image'] = $this -> getImageBytes($this -> request -> data['featured_image']);
			} else {
				$this -> request -> data['featured_image'] = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATIAAAEfCAYAAADC9EotAAAKrmlDQ1BJQ0MgUHJvZmlsZQAASImVlgdUE+kWx7+ZSS8ESIiAlNA7UgQCSK+hSwcbIaGEEkIgqNhQEVdwRRGRpq7gIkXBtQCyFkQU26LYwLogi4K6LhZEReUN8AjvvfP2vPP+c+58v3PPnX/uTL7vnAsApY8rEqXAsgCkCjPFwV6urMioaBb+dwChFwnIA4jLyxC5BAX5gb/Vx/toLao7JlNef1/3XyXHj8vgAQAFoRzLz+ClonwSjXaeSJwJAIIG0FqZKZriUpQZYrRBlI9MccIMt09x7Azfna4JDXZDeRgAAoXLFScAQP6A5llZvATUh8JA2UzIFwhRdkfZkZfI5aOci7JxamraFB9DWT/2X3wS/s0zVurJ5SZIeeZdpkVwF2SIUrir/8/P8b+VmiKZ/Q1NNCiJYu9gdFVEv1ltcpqvlIWxAYGzLOBP109zosQ7bJZ5GW7Rs8znuvvOsiQ5zGWWueK5ZwWZnNBZFqcFS/3jMjxCpP5xHD9pDykBUo4XeHJmOTsxNGKWswThAbOckRziO1fjJs2LJcHSnuPFntJ3TM2Y643HneshMzHUe663SGkP/Dh3D2leGCatF2W6Sj1FKUHS+rgUL2k+IytE+mwmusFmOYnrEzTnEyT9PsAdeAA/9GIBc2ALzAAbRABPEJQZt2pqTwO3NNFqsSAhMZPlgp6aOBZHyDM1ZlmYmVsBMHUGZ/7i933TZwtiEuZyIiYAtui+RarncrHKALSg+0KJOJfTPgwALRKA5hyeRJw1k8NM3bDoyaYBBlACakAL6AMTYAGsgT1wRjv2AYEgFESB5YAHEkEqEIOVYC3YCPJAAdgJ9oBycABUg1pwFBwHLeAMuAAug+vgFrgHHoF+MARegVHwEUxAEISHqBAdUoLUIR3ICLKA2JAj5AH5QcFQFBQDJUBCSAKthTZDBVARVA4dhOqgX6DT0AXoKtQDPYAGoBHoHfQFRmAKzIBVYV14AcyGXWBfOBReBifA6XA2nAvvgEvhKvgI3AxfgK/D9+B++BU8hgCEjDARDcQEYSNuSCASjcQjYmQ9ko+UIFVII9KGdCF3kH7kNfIZg8PQMSyMCcYe440Jw/Aw6Zj1mO2YckwtphnTibmDGcCMYr5jqVgVrBHWDsvBRmITsCuxedgSbA32FPYS9h52CPsRh8MxcXo4G5w3LgqXhFuD247bh2vCteN6cIO4MTwer4Q3wjvgA/FcfCY+D1+GP4I/j7+NH8J/IpAJ6gQLgichmiAkbCKUEOoJ5wi3CS8IE0RZog7RjhhI5BNXEwuJh4htxJvEIeIESY6kR3IghZKSSBtJpaRG0iXSY9J7MpmsSbYlLyYLyDnkUvIx8hXyAPkzRZ5iSHGjLKVIKDsohyntlAeU91QqVZfqTI2mZlJ3UOuoF6lPqZ9k6DKmMhwZvswGmQqZZpnbMm9oRJoOzYW2nJZNK6GdoN2kvZYlyurKuslyZdfLVsielu2VHZOjy5nLBcqlym2Xq5e7Kjcsj5fXlfeQ58vnylfLX5QfpCN0LbobnUffTD9Ev0QfYuAYegwOI4lRwDjK6GaMKsgrLFQIV1ilUKFwVqGfiTB1mRxmCrOQeZx5n/llnuo8l3lx87bNa5x3e9644nxFZ8U4xXzFJsV7il+UWEoeSslKu5RalJ4oY5QNlRcrr1Ter3xJ+fV8xnz7+bz5+fOPz3+oAqsYqgSrrFGpVrmhMqaqpuqlKlItU72o+lqNqeaslqRWrHZObUSdru6oLlAvVj+v/pKlwHJhpbBKWZ2sUQ0VDW8NicZBjW6NCU09zTDNTZpNmk+0SFpsrXitYq0OrVFtdW1/7bXaDdoPdYg6bJ1Enb06XTrjunq6EbpbdVt0h/UU9Th62XoNeo/1qfpO+un6Vfp3DXAGbINkg30GtwxhQyvDRMMKw5tGsJG1kcBon1GPMdbY1lhoXGXca0IxcTHJMmkwGTBlmvqZbjJtMX2zQHtB9IJdC7oWfDezMksxO2T2yFze3Md8k3mb+TsLQwueRYXFXUuqpaflBstWy7cLjRbGLdy/sM+KbuVvtdWqw+qbtY212LrResRG2ybGptKml81gB7G3s6/YYm1dbTfYnrH9bGdtl2l33O4vexP7ZPt6++FFeoviFh1aNOig6cB1OOjQ78hyjHH8ybHfScOJ61Tl9MxZy5nvXOP8wsXAJcnliMsbVzNXsesp13E3O7d1bu3uiLuXe757t4e8R5hHucdTT03PBM8Gz1EvK681Xu3eWG9f713evRxVDo9Txxn1sfFZ59PpS/EN8S33feZn6Cf2a/OH/X38d/s/DtAJEAa0BIJATuDuwCdBekHpQb8uxi0OWlyx+HmwefDa4K4QesiKkPqQj6GuoYWhj8L0wyRhHeG08KXhdeHjEe4RRRH9kQsi10Vej1KOEkS1RuOjw6NroseWeCzZs2RoqdXSvKX3l+ktW7Xs6nLl5SnLz66greCuOBGDjYmIqY/5yg3kVnHHYjmxlbGjPDfeXt4rvjO/mD8S5xBXFPci3iG+KH44wSFhd8JIolNiSeJrgZugXPA2yTvpQNJ4cmDy4eTJlIiUplRCakzqaaG8MFnYmaaWtiqtR2QkyhP1p9ul70kfFfuKazKgjGUZrZkMdNi5IdGXbJEMZDlmVWR9Whm+8sQquVXCVTdWG67etvpFtmf2z2swa3hrOtZqrN24dmCdy7qD66H1ses7NmhtyN0wlOOVU7uRtDF542+bzDYVbfqwOWJzW65qbk7u4BavLQ15MnnivN6t9lsP/ID5QfBD9zbLbWXbvufz868VmBWUFHzdztt+7UfzH0t/nNwRv6O70Lpw/07cTuHO+7ucdtUWyRVlFw3u9t/dXMwqzi/+sGfFnqslC0sO7CXtleztL/UrbS3TLttZ9rU8sfxehWtFU6VK5bbK8X38fbf3O+9vPKB6oODAl58EP/Ud9DrYXKVbVVKNq86qfn4o/FDXz+yf62qUawpqvh0WHu6vDa7trLOpq6tXqS9sgBskDSNHlh65ddT9aGujSePBJmZTwTFwTHLs5S8xv9w/7nu84wT7RONJnZOVp+in8puh5tXNoy2JLf2tUa09p31Od7TZt5361fTXw2c0zlScVThbeI50Lvfc5Pns82PtovbXFxIuDHas6Hh0MfLi3c7Fnd2XfC9duex5+WKXS9f5Kw5Xzly1u3r6Gvtay3Xr6803rG6c+s3qt1Pd1t3NN21utt6yvdXWs6jn3G2n2xfuuN+5fJdz9/q9gHs998Pu9/Uu7e3v4/cNP0h58PZh1sOJRzmPsY/zn8g+KXmq8rTqd4Pfm/qt+88OuA/ceBby7NEgb/DVHxl/fB3KfU59XvJC/UXdsMXwmRHPkVsvl7wceiV6NfE670+5Pyvf6L85+ZfzXzdGI0eH3orfTr7b/l7p/eEPCz90jAWNPf2Y+nFiPP+T0qfaz+zPXV8ivryYWPkV/7X0m8G3tu++3x9Ppk5Oirhi7vQogKABx8cD8A6dE6hRANBvAUCSmZmRpwXNzPXTBP6OZ+boaVkDUN0OQGgOAH7oWoauumjQnAEIQiPUGcCWltL4pzLiLS1mvMgt6GhSMjn5Hp0N8QYAfOudnJxomZz8VoM2+xCA9o8zs/mUZNH5fxRvbmEW1vkqFfyn/gEUmASzW77rFwAAAZ1pVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8ZXhpZjpQaXhlbFhEaW1lbnNpb24+MzA2PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjI4NzwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgrWhwqGAABAAElEQVR4Ae2dac8cxdWGB2LCYhtjFoMBsxjMHiCBiCSgREFRovyr9y/lUz6giChCQlGigBxBwAbHxmCw2SHghCy8vopcT47LPc8zS3dP98wpqaf26ur7nLr7VHV1z2VPP/3015N0iUAikAiMGIHLR9z37HoikAgkAgWBJLJUhEQgERg9AklkoxdhXkAikAgkkaUOJAKJwOgRSCIbvQjzAhKBRCCJLHUgEUgERo9AEtnoRZgXkAgkAklkqQOJQCIwegSSyEYvwryARCARSCJLHUgEEoHRI5BENnoR5gUkAolAElnqQCKQCIwegSSy0YswLyARSASSyFIHEoFEYPQIJJGNXoR5AYlAIpBEljqQCCQCo0cgiWz0IswLSAQSgSSy1IFEIBEYPQJJZKMXYV5AIpAIJJGlDiQCicDoEUgiG70I8wISgUQgiSx1IBFIBEaPQBLZ6EWYF5AIJAJJZKkDiUAiMHoEkshGL8K8gEQgEUgiSx1IBBKB0SOQRDZ6EeYFJAKJQBJZ6sBcCHz99dcTjnSJwJAQ2DWkzmRfhotATV7GL7vssuF2Onu2MQgkkY1Q1JE8JBQug3DMI434tDL/+c9/KFLyKfPvf/+7xC+//PJSj7rxKJnhhzqxDcPWoR2OmN7Ux9BkOR/lbYPyTXWa0minTo9x2iSebv0QSCIbmUwdjA5I4roYNp88wpJJJAlI5qqrrppcc8015SC8e/fuyRVXXFHSiV955ZUl/q1vfauQEr7t0dY///nPyd///vfJl19+WfxPP/20hD///PPJ3/72t8lXX31Vuse5cNanr03XUvebOl4XeYZtz/L6pBv2HLRBGkRtPdLSrQ8CSWQjk2UcpNO67mCHaCgPMe3Zs6eQ1L59+0r4+uuvL/7VV19d8hngEJd1PQ++Yc5Xh6knYehDXpAbZPbxxx9PPvzww8n7779ffAiPflEPUsNZjzDt131oIh/7YXnbMJ22OA/O9iTRkpg/a4VAEtlIxcmA5ZBIjOMzcCGoG2+8cXLw4MHJDTfcMLn22mtLGhYW+dRjoEsmkM+//vWvkkYb0UkETWnUic66Wnq33HJLyab98+fPT86dO1eOd999d/LRRx+VNOrE/nA+z4lvm/h1unFOYj6+YfvWlGZe+uNH4LKnn376Yq0d/zWt9RUwICUfLQ4umHQcA5uDMlogTP9I+/a3v12sMqwzSG7//v0TLDNIB6KhDQgPR1iSsG18CAffNM9n3Hz6Rhr5pNkW1iFTPPr0ySefFFKD0L744ovJP/7xj2LFkVcTK21wPeRF5/lt37jnpA/0xXSvLbaR4fEjkBbZCGUogUkUcRBrVXFZEBekhVV24MCBQlx79+4t617k4Rzg+qRJSp4ntm9eXd50SIo8CY0webRFGlNO0nbt2jW56aabymEZSIqyEBpTUNbYWHPjgOhoG4tSkqKvkiLtUk8fIqRdiNO+UJZwuvVDIIlsZDLFKoEUJAYGKwMUAiMMUWFt3X777YXAsLqcTkISlKUc9YkTxtEujjSdecbxzbcN84jTBu3HcrF8PJ9TUup5HnyIhgcOELDT0rqvntO2iVOGc0N6rM2dPXt2wvT1gw8+KORG3yBv++c5bSv9cSOQU8uRyY/BGy0M4jx1PHTo0OTOO+8s5EXcQStheJmRTLZLM6/2IQAJRDIgbhp9g1RiHmnE6QvhJme/8Osytk0bsW3biefC0sORhoXHg4aTJ09OTpw4UaaxWGjkedi2baU/TgSSyAYoNwYZA9YBXQ9eBh8WxnXXXVcsr5tvvrmUZTEd64s4i/2Uoy18wwO83Na7JH744AS5ffbZZ4XQ/vznPxcrDWzJAxecOBEmjXxxMxxlQrl0iyMwTR+VQ5SLZ4l5hI0jnyQyURqYj5CcBjLgCCMwBxNExRQMK4f1JNbAHnzwwWKRYXU4CBU2/qY4BwEEBj7EIXgcVtorr7wyOXr0aCE3sBJTfMnNNsCNdOK0Rzjd4giAI5iqjxFPsUYGhmN5yzIOvLkg37Lumk8tFxdKVzURXrnL/HcQcR4Fy2BiMHLwtPHuu++efOc73ykERj2EzWEb1CW8KY5rl5wkfwYGDwtcM+MmwDra8ePHywMCpuHg6eACKwdNJLaYvyl4tn2dYBj1kTiHOgveyE39rcuj/zzIQZ7sUfSJd1pkbUtqyfYUat2MdyAGHMI8fPjw5LHHHivTSMpqeVA/OhSCNNut82PZdQlzzQ4Erskw187G3L/85S9lzcwnqORLfgwobyJiZn3yNgG/LvUALMWQsI408QZ/nLIgjt4jLx7esG2HJ9o46+RTywLHcH4QXrQCorDYXnDrrbdOvve97xUiQ4gQGL7lok9bxqPSlMQ1/ZF04vWCD3HwYMvHbbfdVh4CsF6GVcagAHOxx+egngf1xXlNoevlssRQ+XjTUD7KgXQOLC/IC8uLJRScdWM4LbICzXB+vPv41BHBY0pjMTz55JPFCsMiQ5iRxIhrlnM1Kow+A5O2oxIM56rb6wnXyzVyeO2xddLAjWk5eDBI/vSnP02OHTu2VQx8ca5RUieGtwpmYG4EkAu445QTcTAWZ8gL4uKJM2EceZQ3TByd1iWRicRAfITDQJKkMKnZ0PrMM8+UaST5cVBJUCqBZKXgFT6X591uIJfaSTciHl47WODIw0WMwAR35syZyQsvvFAGD/j6cICw5fVLhfxZCAH1kspgD6bouPv/mPoz8wD32ilH5Uq+aUlkNVoDiSNIBPbII49MfvCDH5SBhcB15CFEFYO4A40BG0mLMPkOSttYR188trs2sQInMQQ74n/4wx/KU02mMVht4oxveLu2M297BMCZA11kSwyWF2tebB0iLcovhrdv9QKh5VPLnSBqN5/BALEgNBzCqh3WGFPLH/3oR4XIIDDqpeseAXA/eWED7YsvvlgeDDClR0YOPnqgzPC9aTgIu+/h8M4gwYOHempYfScORjwthrh82mj5Za8qF/uXRXDO+gwIiIkBgmMgcDCdRNCQGJ/aefbZZ8tOfSwD6qAIuLYEXxrLn0sQYFrDGxK8TP/b3/528tZbb21N9bVsqaQc8DmUzyUNjizBa4lE5CU0XSNplkWPjaOzOMmL9S7IC8uLcm27tMjaRnSH9lAUSIwFfMIMDkiMAUScT+78/Oc/L+9LstDJWg2Ch+AoR5l03SEAvt44CP/ud78rU03lEAczYQdvdz3qt2VJybNKTGCh7omBhGQZ6qDPcZ8XBObTxqa2qWO7hBd1aZEtitwS9ZyG+GQSEkMpeLXoF7/4RfmUDmkSFyQWrYElTp1Vd0AAEnPQEsYy5t3Vl156acvyiAPSQU2zMX2H0ww2uybmmqzEBp/rlfSZZaCzvKzP2hc3Yet6sdbBx+mbv4yfRLYMegvURfgqi1NJ0vjSwy9/+csyaFAIFASnJWa9BU6ZVeZAwMFGFQYnsmKtEnn88Y9/3JILceWIbKjX5sCco8utFuVacOKgHhL3ekmjHLoJafHEEcuL9S9xoY3YFnGc7X4T++a3DdySyCKiPYQRtBYWRIZjPQZLjDs/eSoPioAlhgLhSG9D6KWx/GlEAKzBHNmwBECcqdJTTz1Vwi+//HIhOKaaDto4YA03Nj6iRHVN3QMT0iQvXg/iYDOxOsm1Ux7fsJcsLpS1vHn6lJmWZ5lpfhLZNGQ6SkfQKIRPIvmCxU9/+tPyHTEGDM4yDCbKokQoEHEVoqPubXyz4A3+DCh8DjDHSobMsDr4JBDyQy44B986yIbrR8+4JpY2iKOXkhb7vHhViDI4rpkylBeHGKZMnU9adLRR14n5s4STyGZBqcUyCAzBcjAQfvzjH5fXZlAW8hAq6YTxGUgOGtLSdYsAeOOwxsBdWTFwObjpsP7zzjvvlHIOUiIOyJIx0h90DALjWpky8qQREmPBXlJXD7l20ozX108cJ6bmm249/WUgy6eWy6DXUBehROW2SBSi08ef/OQnkwceeKBYW9SzjHXSHx4C3FyYTv3qV7/aGtw+lOmrtw58CaE+r3q0XblYhmsiDnlxbRAXa15dbZWo+9tGPC2yNlAMbUhiNaGRzh1e5XvooYcm999//0UkhiJRLt1wEcC64A9aeGXs17/+ddm4TBqy64vQ1CFQQs84SPPQAlKXJDRRpRx5+NxUIS+IiyNOGy2vT/m6LfNW7SeRdSABhI2iqGCcAhJD0ZlC8oTy+9///pZZLoFZp4MuZZMtIYBMkeXdF74D9+ijj5YXznmVCdlCIAz2Lh06wnnULeI44vr2wTTSSeOgPDrI00YsL6aOTBttr65jm9EnPDSXRNayRKJCoDjGCXP3Y++Y705y98OsVxn7GAgtX+7GNedAh8y++93vTk6dOlXWknxTo2tAuOlJSpxL68s0+2c/0C0O+ou+1fu8qKdTV21D3/wh+0lkLUsH4UNOKA7KrbVFmEViviXGH4W4a5+7I3mQXFSqlruVzbWEgDcd5MyXZp944onJ888/X25Y5HU9+NURzkM49scboWte6CCWFwfTRnzKR/IDlthn2xWumGfaEP0kspalgnKhKChT9DHf+SgiX3UlzFQERUOxIDEcdceiOC3DNqrmXA/Dv+uuu4pc33777XJD6utCJE3JizgHfeJpI9NGXs5mu4g6Rb7l1VP7SxxHWcubpz9k/UwiU0ot+igLRKU1hmIwpWQqgvUFgXFAdjgJj/IqVIvdyaZaREBZaUEj14cffrh8zww5dy0/yYhzoT/E0SWIC9LyaSO6hKM/9om+46jLoaONOs286Mc6MX0I4SSyDqSAgqHokBk+CuD/TjKVJF+lcGAQV6E66FI22RICkEK8QSFfZIu1zd4yZN6lQ0/QH/Rmp31e9FU9s08xrSa4mGf56O+UH8v2He4W9b6vZgDnQ3EQuMpGGOVmLQVHHCVUKfTJq5WOtHTDREBZ4SNf9gOePn16q7NRroQhP8ppFW0VvBCwrXgjsz556JJtsM7l97zcpNrUFmm2O2t+U/lpdWP6EMJJZC1LISqgynfkyJHyZ7pNStzy6bO5HhCQcJCvjs+R33jjjWU7A8SDI1+Zk+Z0j/o4b2jkQSLkU4fDMqTFfV5j2qRaLrKnnySyloGOyo2i8nIxd2vWxnhfL+a3fOpsrgcEtFrwDXNaNskyxeTPTLC8lDM+5SQmfMnNNAmMeqSx/BB32LvPq748267TNzGeRNay1KOSoqB3X9g4yXfGUE6UNCp/y6fO5npAQILSwpJMkPVdF55gvvrqq4Wo0ANkDTnh1At8CcywD4AgL4iQtS/C5Nu+lxbjqUuiMpkkkf0Pi9ZCKCqKjYLee++9RXGJq9StnSgb6h0ByAMykYSMc6PiPzP3798/OXfu3NarS5INPnphXXzi7Cf09SDCpNG2zvoxTl1cnWeZTfSTyFqWusrK0yzWTTiYUsY7eMunzOZ6RADygEh8Ki2hkYaM+fPfd999d2u9i3ynmtTlhsZU0RezsbxwkhPlCXNEQiMucen3eNmDP1USWcsiUvkgNKaVrJHVWy5aPmU21yMCyBUicYHeU0s03LiwxHEQGGXZ5wVhucM+Pm2kHq4mJ/WIPM9JuMl57qa8TUlLImtZ0igdd11eX+HurNJDZih4VNCWT53N9YQA8mUjrJuakTFyRfZ87ZeFfzanYnW51wvyol500whIUpPk1Jmdyse2Ny2cRNayxFXqAwcOlC0XKDtOgmv5dNlczwggX2TJ1FJiwTcNn7Wuo0ePFl9SaurmdnmUr/PreFObm5qWRNaR5JliOP1IBewI5AWalXysShyHjDiMmx/9Ot+2WAOFvPi6BGWwvIgT1lnWePrtIpBE1i6epTWmkE4rbb4eBKan3y8CkovE4nSPuFYzvvn2jrgHUz0sbTanQlgQGe1SjzVRbmKUxXk+20m/GwSSyFrGlYHBn+yyTuK00lMkmYnE6nyJRR/ywRFHdlrRcV0KUiKPQ8sL64sy5El8xFkLZZ3MNkvgv+0bTr99BJLIWsYUZWYDLAMCIvPO7GkcQMbTXy0CTYQlOUlMteVFjyGvJtIjb9++feVhD/WQd8ocVLp1SWQt48sgYKEf5SXMEQdLy6fL5pZEAPngoo9VBQnxpJFpI/IjX+KSmPRjXdpieolFjvUWHeWsE9MzvDwCSWTLY7jVAkqKEnNHJhyVNoa3KmSgdwQgE28sWFUcTBmZKrpojyXtU0nLID/ScBJX3XllDOGxw5+NsdGZH9My3A4CSWTt4LhFWvxbOGTmgMFnMKQbDgIQjdNGrC6sL0gsrpFRBtlBPpH4iJvuFTXF9+zZY/YW8SWRbUHSeiCJbAFIUVyUG4LSR0m5Y+/du3eye/fusj5Gfq3kC5wuq/wXAbDESSzEZ8HXesgIwsL6qqeNlIG8cLF8U7wmpDqO3CGy2Le6TDlR/rSGQBLZAlCilCirvk2guFhk5HE44FRi61g+/ekIgBl4RlIBT9NiumVpjXTxNgx5RcuLfOsgE531jC/js/M/XX8IJJHNibWDIw4GBwA+ayMxj+ZjnTlPt5HFwUvMIC/w9OYAIMSZBuIoF8moJF74YZ0L8nKfl2X1qYejreibXhKX+OG/LtP1h0AS2ZxYM7AYOFoH+gwIlNcnlpajeQclZdoaKHN2e1TFwUmsJCniYs3FkA6WYguxMbWHvJg6cpBGOY7YZg2G7ZBOuWUd/WSdVNdGm7aVfjMCSWTNuMyUKinhM2gOHz5cNkM6aGjEweeAS6WeCdpSCOx0kg1pkhiYu1XCBXvrUIY1L/CmHE4Z2KZ+lBdllnHUpw9xvU2ZTzv/MufLut8gkEQ2pyaojAwUFDbG77nnnjLI3AhLHg5FNjzn6Ta2OHg1YUZaXPOCyCQvwEIuukggpNme6ZaLeTFt0bBExvli3xZtL+vtjEAS2c4YXVQC5awHAnd8vg7Kn09AYhIXvoRX17mo0YxchABYQQBgxwG+kBcHa15MITnEmTKSFPUIN8mJ8srBMpzYNNu7qDNzRmwjtm9fPM+cTWbxGRBIIpsBpFhERXWgESfM/xqyLgKRMbBIZ7A5xXCAxbYyfCkCDHrwAy+Iyx32WF4QGvk4yxFWBuYR56gdaXUZy8X0ut68cdrymLdull8MgSSyBXCLA4LBxaN2iIzBhyPfMg6UBU4z6CoM1GnXZrrk4IVMq2M56sUd9hCZNwDKuOZle56HuG2Y1+Q3lWlKa6o7a5p9ilaiabO2keXmRyCJbE7MUFDIS6sBn71jbruguU1Q3EgwQsh1b0cM1AEv8aEsYaeMPm0kzbYobznP6fmG6NvX2DfT9GNehttBIIlsThxrEmMqyZYLvkHG9CcOPJp20HqaOm76mHwGpIMSPHBeN9Npw6R7vdYxDdzYKsGh5WU9yuqobxvWNW+IPn2HcNP1i0AS2RJ4M8BQWoiMQcifTRCPA4/m6/gSp1x5VQmMjhD2L+64bg4xIIwDFxxxSI8/4dDy8qZAGQ5wasJKYovnLo0O8If+cy1cm9dS+wPs9ui7lEQ2pwhZp8GawALD+ohfu+BfcnjXUsWNTTsYY9oYw/HaJCuug+vj0CIDJ/JdsHeHvfUp64MQ0jh2woj2diozBEwhMh9OELbfXvsQ+rhufUgim1OiWhEqJ0R23XXXlX/LOXny5OTRRx/dIjIGXRygxuc85aCKOxi5lkgqMQ7R87QR8sL6EisGdcTAtrhA24ppTRe+U35Tnb7T6CPXHPsar7vv/mzC+ZLIFpAy0ycGK8rKVw54Nem1114rFsZ2AzIq9gKnHUQVLQyuhTAH5A5hYYVglWKVceDI92kj5cRH34sSG3zz9ClDesyz3tB8+oheYLGLlf0fWl/XqT9JZHNKk8HFIIXMsDr4Pjv/Yfjmm29Onnnmma3WUOh1dFy/xMS0sV6w95opx+HAJszAlowiPpar84hbjjI4455nqL7fpUNHcGPp91Dx3KlfSWQ7IVTlo5BaIQxoppWvv/56sUr4vLGWiAOvqt57lP7WfanTHGR1OTtrOuUYmFhfrnk5baSM5axXt2tcf1o50/HrNmPeEMP0F8uTP2jmu3QS2RD7uk59SiKbU5qSmE/nUNRjx46VPxwxbc4mOykOWTCo6K+ONK0iyYQyHDFuedOn7fOiHO1Zznqb7IsxuoBVltj0ow1JZHPirKJy12UdhAV+yIx1Miw00ofgHEAuOks4rO2Rx4GLFhVxro80yjltxALzuq1r+6RzpPsfAuCBLkBkOLH6X4kMtY1AEtmciKKkKCbEAGl9+umnRWkhNQhgKC72kz7RNw4sBfI4cAw4HNfCtNgnjZAz5blODhxx65WE8CPBTcsPRdc+CE7qCHgQxuEnPt2IP4lsQVxVSIiBQc82DNMWbLL1agwonUQDYUlO5GNt0X8JzGtwIFKfNA7bsE3zjFvX+Cb7YBHxr7HaZGy6uPYksjlRZTDjUFSsGZQVYnAQ68/ZbKfFIwHRv5q8vAYtL6/Pa4nXbF6nHR554+LI2mLi1Y8wk8jmxBklZQrGVJJ1JOIQGnuoHPhzNtlqcUiHfnDQNw6sMMiLgcUrQvTbp6vkOy3WWqNDklfdOa2MSI6U8ZzT6tXtrGtcHMASrJXHul7vUK4riWxOSaCgEBdEgI/iMrghiqEMYsiJPtX7vOi7BEQZHP2PpEZ82uAzz3rEo5tWL5bZhDC4cGODyMQoselW8klkc+KrQkIEKKnTMp7wqbT45tM8dSxnmXha0yg3i6N8LEvYc0CoHK55cV7z6VOsx7nM87zm65tu2RhvCjfVayq3rmnIhhvcxx9/XCxfr1MZG0+/XQSSyBbEU8WEHLB0WDDnLsziP2lYOZIEZKIjjbocEott4ZtPeeOmWYe82DbE5VYJrDDKceA8B+GYTjxdNwiA+SeffFL0Ip5BOca0DLeDQBLZgjhCCpIJPkQGmbC7H2LDkR5dTWjEJRzbggCbyMd82qMOpMn5tLxiO5Tx3KR7xHTC6dpHANzB+7333rtItsqn/TNmiyCQRNaCHqC8LKDzziXvXkJqLqBDVhATTiWXWIibRxoEWO/zogwHeZAc6y60jxVGGvVjG7RjHc7pADLNOHnpukEA2bz77rulcXDXJfYi0b6fRLYgpiooygmRQDIffPDB5NChQ1vEQx7lLBMtMk5LOmm2hQ854dMmeS7Y15YX9SUw2/VctMtRu6a0ukzGl0MAmbA+9tFHH5WGEvPl8Jy1dhLZrEhV5SQNklXW999/vxCaWzNIJ8w0MJYnrCNsHB9SqqeNkp1PST2fbUSfNqblb5cX28jw4ghgUUNirpFGWST+i+O6U80ksp0QqvJRzCaF5E7M60pM/fhXJd69xLri0FHPurRDHeJaXlhfTE8hMgYCDvKSwBwc1Kkd7TlopuVzHq24un7G20OAhX6csrZl5WM8/fYQSCKbE0uUUwVFMSEZ16ogoFOnThUS41/HcZAPd2mnjKRJNPXTRtuhXUmO8tYlzcGAz2F/KKezjHH9WN+09NtFAHlzM8Nx48CpLyWSP50gkERWwarS1SQhOcR0wpIMYYiCT/rwjbI77rijTCtJQ6G1qrC6WKxnzYuw9TkvZXGEcZ6zjk9LL5W2+bHeNkUyawcE1I+6mOnIeppFVtfJeHsIJJFVWKKIKKXkEe+q26VBEuRjlfm5Zywx0yAu7tSQF2UtD3nZbhJNJYwBRqfJyHSeXmNp45SxecjZ8AAvbdRdSiKrxAexRGWTaEjjkHSsZlnSOVjcZ38XC//79u0ra16QmPm2YTu2G9ux7fSHiQCyi/IyjNXNOinrnFG+XIVLEMO8ovH3KomskmG0yFDQaJFRVKUlLDlBdtyJsbYgMJ5aUe6pp566qL7KbV1827Mt4+SlGx4CUT6G8ZUfVjdWuXnoBjrkEoLpw7uycfcoiaySn2SDwkUFjXHurpRzYZe7MOSFrztz5ky5C6PEKDNOZbZMrdR13HLpDw+BJlmhEywrxDzCUaeGdyXr0aMkskqOKJ7EQ1hFhLxIh4xQVjY9sqjLNEKrzbsvZdkcC7HxBxQSmArNKa0T0whzvnTDRkC9iL0kDTmfO3eu6Ily1I9lM9w+AklkFaaQkcrHYj2kxLQR0oLAPvzww/LUEWsMB/lIQJITCq1SHzlypIRpk3KUwbduCVz4IT/mmZ7+sBFQV+glMj98+PAEmaMnx48fL68qqR/6w76icfYuiaxBbu77griYMkJiLNijqLVTkVVSfcrxvp37yUivicq6tkmZdONFAP3wFbU777xzcu+9906ee+65yenTp8tF1fIe75UOr+ffLN4Mr19Te+RgVymIe1CJdOOEYzkbrdMojyWGz2LtW2+9NTl69Gg5CMfpo23Uvm3q0xb/sMQDABzppKVbXwRcWsBaZwsG/6L07LPPlv+4XN+rHsaVjc4i06qRFIjrSOMwzTgkIsHgo3AqHWV5ysSaF9OBerGWtiUh2rMdz1n7tks5rDgOPu1jPf26XsbXAwF1Dz2A0Pbu3Tt54IEHJi+//PKWDqzHlQ7rKkZHZChIJAPiOpQoxkmnLAckRB4Ha14QDFNGyAsrTAW0LcpbP/rmT/NjH372s58VRbbtmDetfqaPGwFvnlwFU03eu+Utj5deemncFzbw3o+OyCADlQWCkiQkqkhyYA9xkcfdkY2qrHlxNE0XKSvx1e3MKkfORZ8eeuihskaCtYdCs+5m+7O2leXGhwB6wwMiZI68kT9TTNLQi0X1anxI9Nvj0REZRIFTIYhzEJfkUCAOyAvC8mlj3OdFG7Zle5IicZztfhOb7VdFfvjhh7c2RkJiKnE852wtZqmxIYAOcKg/vO2BDrheOrbrGUN/R0dkKAgkhZMcJC580iCv7fZ5oWAckbhUuig0zrWI279//2TPnj3lHN6Z7feibS7Sj6zTPwLoEU4LTOss6lr/vVr/M46OyCArycDpmvu8ILC4z0ulkqRUJurbhnl1XNHbhvmmb+ezCZbpBP2inufFQpSEt6ufeeNGAJkjZ5cUmAkQVtfGfXXD7P3oiAwYnaq5zwtFYfEeoqidBDRNicynXlOZmF+3PS3OkyrqcWgl0naS2DTE1icdOXPj4kDeHDxU8ma2Plc6rCtpnchqMohxBjZxDpyD3XiEJtbDPCfOXY0njBBXfNrYVN/2bZNz7eRmKbNTG+Tbn3h9KnZb55ilH1mmfwSUrz46yz8q4aJO99+z9T5j60TGgHUgA128E5HOYRph7lgKnfKWwcdhZUFe/qHDsvu8SqMd/ziN8DRen77p6a8vAsiaRX50l/cvcSn/7uTdOpFJTAhNorL7kpwkRbpphEn36Q5fUfVpYxN52YbKoU87q3T0i0fuuLpPdXyV/cxzd4OAeknrjAU+HoD+kp7y7wZzWm2dyCAmBKjgtrO+6ADTRsqzMA55MWVkTaGrfV6cs2vH6ylYZek2DwHJCv1H951Wku6Y2DxUur/i1okMUsJFgSpASQ2ri7Sd9nl5+XX9Ot34EHyuG0KGmNNtHgKRsNBvPhyAvjc9iNo8dLq74taJDLKCeHAIlQNyc8GefEztWfZ5WR+/yU1LbyrbZxpExsEWDK9BMu6zH3mu1SKADqDnkthQ9XW1KLVz9taJDNJSYNyJIDD3ebnmhYARroTnIJcEHfxcIu1pyRG3LGGcbXjOb1JX+8sObq6R/WT0fUh9Wy0ym3F25I3us8jvemmtt5uBRH9X2TqR0XWEqOXlJ6Dd51UL1EEeCSmGI4nRtuUJ4+r4N6mr/WV9TAW2f/qr7VmevQ8EkDU34LNnz26dLuW/BUUngUJkEgekYRgf8DkIN8UlGYgLh5UFYTXt81KQ+pSfJUy5sTmwAgd8HNcZLdWxXU/292IEolxjjnImnxsZFpk6TprhWCfD7SBQGMgnbEwDcRBUFAphhRAHpJaX+7xYD3CrhOVpL4aJr7vjeusXhDcNg3WW8XayJI9xxFhg6wVOEquXSdYZo76vbRcgQ0gIAAIDbNJwpHEQ90BI3G20vKZ9Eof61KH+JjmvmTUynPhtGg7rLHNl7DUSrx1PK13kN88ZjPH020NgF9aY5FULiNNAXOQjFAanG/ywvLTkKGddfdI2efA2EVmTwoNTunEhUOt1jBOGsE6dOrU1JmL+uK50PL3dBVEJPt1msJGGD3n5PS8sLzaqRufApL7C0o95sc66h7l+rp1NsfUdWGzWHYNNvj5u+mzqzvWxfrVgF4ONgcf0Ep/4vPu87DL1Haz65m2aD5E5tQALDvBJt34IRNkyjo4dO7a1IRpic4xt+pjoUvK7+KY4U0SeNGJxYXkxLWIQOvAkKC0MByYds4xpMV7nd3khQ2obLPisNgv+V155ZZL7kITTYV/QfcYR/7yFk8QIO4YIp2sfgV1//etfC4HNss8r3lFqwqJrTcKyTlNe+5cznBZ5IMLNIBLZcHqXPekCAXQca4yxhPPGT7rhLs6bbV7Yu8qiZHQSD2nTwnWe9WN50/S3y7PMuvgoLtYY64t88hol5u68SRisiyybrgM5ImOcYeJsQ3rttdcussQsUwrnT2cI/O+/1C6cQuF0drYNaRjlhrjYS4SfJLZego/yZMxwkHbixImyNSlaXzmm+pH95RHotBjaAx0s2arC+iMYo9wR6/bOlC31iYAWGOd0vCBXlhLeeOONi6aQpFumzz5u4rl2KZgEvD3xq8AQGVNMnwiDceLcHs6raAnZun9SOSNTtlyw0I8jPV2/CJSpZQ6udkEHT5QZxY7TSwdAu2fL1vpEANlqXTtumFaePn36om6QZ/5FGRnpBIFOvn7RSU9H2ujJkycnt912W7HMvIOP9FKy2xcQiNYWhIa1zdNpXknSJYGJRH9+WSPjjhIF1N/p1/NMvi2Bz54i34hIjMcvb0gKOcYFfSxvp5Xjv8JxXkFZI8s7SLvCcwqJz8ZYph0PPvhguXMn1u1ivYrWJDIMABw3KmRN+nby3Sl/FdeyLue8aPvFulzUkK4D5WWTJIv+2yn5kPqcfdkeAeQIiWGV4Tut3Em+O+Vvf9bM3Q6BXCPbDp0l8iAwHMrLxlieYB48eLBYZUs0m1VXjAByxfrSGuN1Pl7rS7daBNIiaxn/SGA2jeL7J62mpT9OBLTGkDML/dykXB9T9uO8snH3OomsZfmh6FGhDbuO0vLpsrmeEZDInFayf4wwciYv3WoQyKllB7hHhTbMi8QqPKc0XaKLaR10KZtsEQEsbOSHf+bMmSSxFrFdtKkkskWRm1Jv2p05/mEvg0Aii82QFokt5mV4OAhwQ+LzV7wkzton8XSrRSCJrEP8IzFF8ookFsN0pY532L1sekEEXOjnyzF8QDPd6hHINbKWZQARYVVpWRHn4K4tSZnX8qmzuR4R4Gnl8ePHi0yjvHvsQp4qIJBEFsBoK4hiS2i2KaHpmx4f5ZuW/uoQiDcZZMXbGcgoppP2yiuvlKmlsqZsutUhkFPLDrBvWjPhfTw2xbK24vt5DIgrrrii7C2LA6WDLmWTMyIgISEPppBMHfnKr59+R35MKV9//fXSImUi0Vl/xtNlsZYQSIusJSBtRkLyTm06ys7i8FVXXVX+mAIyQ+l5COCgsWz6q0UAeUBcLuojIwiLmw5PKV988cUtixu54mp5r/YKNu/saZH1JHMU/p133pnceuutRekdAJyeQeBg6ak7eZptEIDAtJQJYzlDZFhiv//97wuJWZ10ynAYNi/9/hBIi6xlrJ1a6ENSHDi+hPH+++9Prr766jIYUHzyIDXDLXcnm5sTAeSA7CAmZAOJYZ29/PLLkxdeeKHccJAX6ZajDk6Zz3nKLN4CAt+60Mb/tdBONhEQkLjwVW7CWF3sBL/xxhsn11xzTSEw8hkIDJx0q0cgkhSyQW6QGC/+E5bo7Kmyk8xMT79fBNIi6wjvSGKcgjiO9/J+85vflO+7E2cA5DoZSAzDRaLC6oLY4u59p5CQHPlabvjetIZxJZvVC0ZXPjfuUOY1oXkqBgxW2Q9/+MPJ/v37y4BwUFgm/dUg4E2Hs/OO7PPPP7/1ZFKyQn7IK7ppso5lMtwNAmmRdYPrVqsq/lbCfwMMAp5i8tFFBwV+utUiEMkIiyu+7B9lWZMYvY75q72KzTt7jpwVy5zFf59YNg2OFXdv404vGeEjj/itsWipbRwwA7/g3H6xQgExMPiXJb6Mce2115aB40BaYbc2/tQSFutjPJxJN3wE0iLrWUYOEk/LXZ8vKKQbDgLcTJATu/q5ySizvMkMR0Z1T9IiqxHpMe6A4anY3XffnWssPWI/7VSQlnLhCTN7yNINH4G0yHqWkXf1eJdnHYb3MHOxv2dhNJwOCzkSWcqkAaQBJqVFtiKhOFjwITGml/yRL+sy6VaHQJQLT5V1WmrG0x8WAmmRrVAe0Srjz0kYROlWi4Ay4UmynydfbY/y7LMgkEQ2C0odlGHAMI1x4LBO5jYMT+f+srqs+el3gwC4YxlDZE4t8ybTDdZttZpTy7aQnLEdSCkOCsNfffVVedR/0003FXJjIEF0kpmvy0h8M54uiy2AADLhpuKnfBZoIqv0jEBaZD0DLnHpS0wQ13vvvbdFXBCYZWq/5y5v5OnYepFPLMcj+rTIViwrSEorjXUyBg+kxocXtcbIdxoqqa2422t9evDGQgZznPJZ64se+cWlRTYAAUpO/Gs1O8khMciMdAeTRDaA7q51F5QFRJZuPAikRTYAWXnHZxCx6H/LLbdsTWu0xuimpDaALq9tFyQy1sh0phlPf3gIpEXWs0wgptoxUExnevnll19ufYHUdOrkgKqR6yYOzrk+1g22XbWaRNYVslPanUZGprMJk+mlj/3jlDKS2pTmM3lJBMSYqb3hJZvM6j0gkETWA8jznoI/KcEiiGQG0Ul287aX5WdHwOl7JLEY3q6lWG5a2Po75Vsu/dkQyDWy2XDqtRTfKGOxmX/ywUFgKD5Hklm3ohBj9u2JtT5nNt8wvvn45tdhyuG2yzfvm5L5Ow8CSWTzoNVDWZSZdy/ffvvtyZEjRwqh5WbYHoAPp4CEvImYLMnomy6J1XFktmfPnsnu3bvLeifrnnx7TouP8rGuW21sJ/35EEgimw+vzkur3CdOnCgvkfMv13GdrPMObPgJwJ+DfxTH1cSFLOKUvy5D3t69e8tnme64447SButtuM8++2xy9OjR8gc0JSF/WkMg/w6uNSjba4jBw+N//JtvvnlrvUySa+9M2VITAuDO1J7/U2hykl3Mow4kdvjw4cljjz024VWzuIWDfK2zs2fPblljpONSthHN+cNpkc2PWac1UGyUmkHBH/ryz+TXXXddITPSVfxOO7HhjYMzFhkbk2syUgb6yAMrjf8qfeCBByb79u0rMuIVJxx50Yq7/vrri2xJw9GOTtkbT392BPKp5exY9VJSxUbRsQrefPPNQmIoeZJYLyIoxAOR8Y/wtZNsnF5SDguMv/WDxJCfU0nq1nJDppSxvu0TV/ampT87AmmRzY5VbyVRau/ibMW4/fbby1TFu3hvHdnQE0EoLPZfddVVZV0LGCQwISGOXO67777y/6Q8oIlPOi0fZUa7bKshTdKynO2mvxgCaZEthluntVRyfBT9lVdeKcrPSYnjzGNQkKZfMvNnKQR8SsyUHleTzQ033DB58sknJ48//niZgmJl+X7stBMrL8rqYruR8MxPf3YE0iKbHaveSkYi46Tnz58vU0zu/nHNBuV3SoKfu9GXF5GEA8mwfQJnGlbavffeOzl06NCEp8nIxa+UIBfykYHl696Qbr7t1mUyvhgCSWSL4dZ5rXi3RvlPnjxZXiZncDE9IV8HoWFFxDTz0p8PAW4I4AtBseblNP/gwYOT+++/vzx5BG8sKzAnDO7W20kGtJ2ufQSSyNrHdOkWGQze1fVZg2Hhn4VlBg3pOAcSZBfTl+7EhjeADLCw+BLJgQMHJhCZU06wJp84clBGQCaRKcMII2lJZBGR9sJJZO1h2VpLkpS+DbPwz74ytmRAbDgGBweExpFuOQTAEIJy8Z4bB9YZcQiMfAgOV5OSNxLlpm+PiNNGuvYRyMX+9jFdqkVICadP2AGBf/z48fIP2Aw2yzj4KJtueQQgG8jLGwQkRhiiYrsFBEYZ4qYjD+SAjOJR94Yy6dpHIC2y9jFdqkVJq6kRBguvubBR9u4L/0zOQNJKoPx2dZvay7RLEQBTyEbLCaKSmMhzbYyalCEfJ4kZVxb6sZ1S4b8/phON4VgmwzsjkBbZzhj1XoIBwwBwUOg7cN54443yxMy7OwTngOm9s2t2QjAGS2TAgcOXZJRNlI9yiHk1LMpHv87P+HIIpEW2HH6d1HZgqPT6nIwwx2uvvVb2MRGmPIMIX9LrpGMb1KgyEPOdLl25WI547eoy5JMmSTbVqdvIeDMCaZE14zLYVJSeQcY3yz744IOyMC2J4adLBDYRgdT8kUnduzZkxt4ypkKEXWwe2eWsZXeV0awXN2/5WdvdpHI5tRyhtF0T49v+HLwyg8sBsXphxql9LY86vvrerk8P0iIboSyxwDgYNOwtg9h4RSYOohFe1lp0GbLyqC8oiaxGpL14WmTtYdlLS5BVHBAfffTR1jt/5EFw6VaHQLyZRDnZo5hvWvrLI5AW2fIY9tqCg8PpJZszITM2cCaJ9SqKxpMpH/1YiDQeyCSZRVTaCSeRtYNjr60wGNxTxuD44osvpk5neu1YniwRWBECObVcEfDclb1r890rvrSAY+c+xBS/W1V3UcvL+vxDD2HjdfmMDwMB5MNNKOXUvjySyNrHdMcW49SCJ46PPPJI+awyVhYH33uHzCA1/kKMfx+vX1CObUBsLPY73dyxA1mgUwQgKm9UUU6cNPf6dQN9Elk3uG7barwj8xLyNddcUywwCAlF5xPLfC+ez8eQxkvLn376aSE2yI0jTi05mXf62Pa2ncjMXhCIpEY4iawb2JPIusF1plZRaqaQHFhcWFQQlMpOGkQG2fFdLD7hI7ExnWRnPwTH/yj6qRnrztSBLNQZAvGGEsO1hdZZBzas4SSyngWuIqPckBJTSCwurDBJjDyVX2KS1KjPZ5Ypz1+QSWx+lcF6PV9Wnq4BAWShvM1WnsbTbweBJLJ2cJy5lZpoICDWxJhKSmR1Yw6IOAggNtKxxLDYCDvdrOtnvD8EIC7lxVnrcJRhf71a/zMlka1Qxio9FhmWFUpumt3yjs6AcFA4DWUqShhnXeulvxoEkBFOvw4js3TtI5D7yNrHdKYWa8JS8SG06Eg3T1JjMBDWAsvBERFbfVh56dsj4nnDEY12/SSydvGcqbWaxCQkFF2y2qkhy9Zt7VQv87tFQPLS58ZkWL/uQT3dRKaz6kFsa5E6sf6Ywzm1XIH0okITRpG1xGLeCrqWp1wSAeRXE0qUqdtteMiDo6yyt14sH7vTlE+a5fVjnU0JJ5GtUNIqoYv1dGWTlXGFomj11E0yNI1/YHr00UcnbJ85e/ZseU+WBz48tHG9s+5ME4FZxnaJq0/mbZKfRLYiaaucWGMSWVTKFXUrT9shAsgXuUNmvJZ2/fXXlzcy+Novb3Bw+E/y6gd1mvSiibSaynV4OYNqmr+AufTj4oPq4np2RkVk28UTTzxRppebrIjrKeVLr4pppA9rDHMzQ/bnz58vZMZGZ0iN/EhotKbeXNryZqckka1Y/uzWv//++xvvuivuWp6+AwQgIgkK8uJwsZ88SI6HP+wt5PNMWGuff/751jpa7FJNcjFv08JJZCuUOIp43333ldePfHK5wu7kqXtCACKDvCQwyAz5a5mhF4bJYw0NK+3cuXNlbY2yTY56lN9El0S2QqmzwPv4449P9uzZUxZ6N1kRVyiG3k6NfCUxCEfSUe4SmxYb6TqJDeuMaSf/1cADg2mkZr1N8ZPIFpC0CqYi0oTKOEtzTh8OHjw4ueeee8rdlydWKvIsbWSZzUQAHUHXIDtIjeknxCapRT2M4YgW6biovzGf8LS6dbmhxJPIlpBEk0JspwCx/O7duydHjhyZ7N+/v0wdvOMu0Z2sugEIoEOQGA6LHr1h6smnnfhu3XvvvVfitaWmXlLe+rRRx0mLjnrW3Y74Yp1VhJPI5kRdoTZVUykog4uCR+n4agXExccU+fQO5f33I6y0tMqaUM20iAA6VesXcXSJAwLzg5xYamy8jXpIW5TniIRWtxnPOYZwEtkCUppV6JAThMVnrDmuvfbacjYUDtJCwSA4fOKGF+hSVtkQBNC9SExRF0knLqmhUxAZhMYUlKlok6M8dZvapXxMb6o/hLQksiWlEBVLpYKwsLzY9MhCPoTG3U+FwPfuyemtF9tasltZfU0RQHfUFy5RnarTiHtQDlLD+mc7xyeffNK4pcM2bJN6ONLrtG9yhvObRDanLBQ21RQupITlBXExbeSjh1pXkcCsi/lPHeO0RbkYJy1dIlAjgI6gd1H3KGNabV1RnjQcOsZbBXw2is23TEG11MiLTl30PDFviOEksjmloiJRDfLiK61MG9mhz6tG3PlwKpYKYZqKRb6EZp6WG/F0iUATAugNOhT1MKZ5Q2zKt72og5Rn822cftakFutxriG6JLI5pcIfhUBeWF6QV215RQWK4XiaqHhRMUyPZTOcCEQEpulULGN4O32yHcpwAyUOgbGNg6kn+9RYX6ufftr20PyNJzIFqmDqOOlMFSEu1r1csLdcJCLbSD8RGCsCkhoEBqkx9eQgXFtqjgGvNcZj2Pwu/Y0ksmkgx3Smiax5YX0xhcTyIh8B4+uSyEQi/XVCAL1mbQ1i4yEB/9bFwWtSLp9wvY6FaeMgjqku8VlbImsCeCdQERyfVmHNC+sLSwwHeSnYaQLrUkjZdiLQJwKOHc6pvjM2OFhPY9rJ00824Jq/09jquv9rR2SAHU1ghTINcPJZ62Lq6BNH2qA87ZCvkEizvVowpnueOj/jicAYEECPXReTvBgHUa+x0iijhcaaWhxz9XU6nmIbdZll42tHZBEQCci0GGfqiPXF1NG9XgAteXUJuv1JPxEYGgLoPcQjocU4YdJ1lIPQsMz42i1f6IjjJo436tRx22nDXzsiq8GKwAMY00YX7v0yq3cT6+pT3voKNAqK/HSJwDohoO6r58RjmuPBa4bMcKybsT/trbfeKpttXUfTGrO87Rpvy187IhMYwSfu1JF1L17WjuCTb7wJZAXXlEfddInAuiGArqP3jAtu8s5SmFJ604/XTFkO8ngoxgMBXl7vcw1t7YgMQBEEoMenjuxoJs/5v8KyvHF87yKm1WWiEDOcCKwbAug7TgKLcccG+U3jgjHDWONJp4TGA4JYvkRa/hkkkQmQF48vqUTffNJ0WFysfTF9xBLDNd1FLJ9+IpAIdIMApAeJnT59uqyfcZY4trX4SI9h4vO6QRKZFyH7R6IizzsEYfIo53uOrIFhjQlYXZc66RKBRKAbBByPtE7Y6SjbNd5+++3yrTTHpj2QxPRNn8cfHJHVF+nFSF6RmNxxj/XFq0OA5iIj5SxL3Wnt2n76iUAisDwCjDOWb+KY8zU+XlQ/depUeQWKsWo5zspYXWaMDo7IuKjIzFwcTlIijNXFtgmsMD5WyNSxae2rrkfc9ginSwQSgXYRcF2NMYwjztglDnmxdoZlxsOAepzHMT5vrwZHZPXFcUFcoH9qetNNN5Unjy4oAhQAQVBYY4QjIBIXaYRj3rxgZflEIBHYHgEJDMMijk3COMYtYaaaJ06cuGhMxrG//VkuzR0ckdVdZPoIeWGBYX1BRFpfkpZxTFjDEhjtRfKK6fW5Mp4IJALLIcBY44CUOIzbKiRGOns4+XTQ66+/vjXFjOPU8rP6gyQyyIYXtSEwppGwOA4QolXFhRM3zThl6zTLeWegTLpEIBFoFwHGIESFY6xxOGMiLLkxe8JI4VNBkBmbaZdxgyMytk5AYHwuRzLiAhdha4nNdgRyGcCybiKQCExHwLHm2IO4mCURdwZlmHRmUZDZG2+8UbZqTG95+5yFiIzO4ujQNOcFxfymeqRxMb73yD4w29WPbWQ4EUgExo8AY1vrDIKDzI4fP75lmTVxxXZXPTeRNRGUJyCPA8tH15RGHumYlnHzKmmyt/XTTwQSgfVBAAJjnOMgMrhCjuBfniAzt1BZzjrbGTZLExknazpB7IRikIHZcR8JjI5DYOSnSwQSgfVFQL6QnLhSyIxZGeOfp5knT54sAJCOtQY36JeMhp+5iSy2Yad2SiOfjvKk4pZbbinrX1hjEBid5QJoiw7jJMESyZ9EIBFYOwQ0fjRetMx4sMcXNM6cOVN4QMKLfhMYCxNZJDGJx87VeXzv68CBA8UKI49OW5ZOGeaiDDd1NtMSgURg3AgwvuGAmiPiuMf6OnbsWPlwI+k7WWMgshCRxU7UsJqHz9aJm2++uWylgKTY1Us6R+28kKa8umzGE4FEYLwISGZcgeOeMGMfIwerjJfNWS/jtSYc/EHeNLcQkcXGOLmd8WSuf/nlVc1G6jVdRGwjtp3hRCARWC8EHOvwAHyBi5xAPgYPm9/5bwB2/8sv1m1CZCEia2qQTvHi9m233VZeIcIcpANOI6kTXexcTM9wIpAIrC8CjHu4wvHPlRKWU/ANU46XzPmENmkLWWQ2xoliOMZNhz0PHjxYXiPa7mTUTZcIJAKJwHYISFr6r776avn8j3XknRi/xCKrC1kYP+YZ5gsUhw4dKnvC3P8R62Q4EUgEEoFZEZBXKI9FhmHEHwRjmcW8mF/C/EQXTT4qcujM0zRkK8U999xTFueY16ZLBBKBRGAZBOAYeQYf/vFPsk1van/bHaixUQlNlrz99tuLJUYZ9n+xT2y7EzWdPNMSgUQgEWhCAJ6J+0rZ/SAH6cdlrG2JLJ4gVr7rrrvKtgqsMBqDxDgpJ0+XCCQCicAyCMg1tgHPsITFoavL7DKj9i2olQVhkcZ0kgaJkwd5sTZGnmXqtjKeCCQCicCsCMAlGkbyD3XZVM+/mzfxzI4mlISGz7fxb7311q0pJCfhhGy1wKVFVmDIn0QgEVgQATiFQ0OJMNyDscSXcfhvWpzpnuYSIpO4bNCCvOjNHjGJixNBYE4rKUdaukQgEUgEFkUA/uFwH6p8hA8nsfDfZDBdQmR2wAapTKN33HHHluWlaUeeYep5UttIPxFIBBKBeRHQiJK8qG8abwvx6qNp+JS7ZI2MCjrCFOKJgf9WFPMtl34ikAgkAn0ggOHEK5B87x8DixkinDTVIrNTfjufSjl1FJX0E4FEYBUIwEFYZLwOGfloKpEVlrvwRJLv58t8q+h4njMRSAQSgYgA3AQv4bucdcnUMs5LeULAn4BgvrnIT+V0iUAikAisAgH4iSeYWGU8aPS1yEssMomKQmy3IC7zmbeKC8hzJgKJQCIgB/Hk0q0YoHIJkQmV81AsMVgwWmqWST8RSAQSgb4RgMRYH2NjvlsxLiEyCEu2w5fAYEIr9d3xPF8ikAgkAiLgDJH//eB/QHAXEZmkxboYu2h5xwnysqJmnQ2mnwgkAolAnwjIUVhkPIRk+Qt3yWI/if7LN5V8j5J0XJLZNzjkbyKQCPSPQM0/vHEEoW1ZZDId5hrrY+7RSGusf2HlGROBRGA6ApIZHMV+Mjhri8jMZAGNJ5YSmVNL8i0z/RSZkwgkAolAtwhgdOGcXvLaUiEyMyAtppWSGMSlpWaZbruYrScCiUAiMDsCcBVvH21ZZFRlvsk7lW5+pZBWmIQ2+ymyZCKQCCQC7SIgH9EqnARXwVsXLfa7yE/hWIFKxvVJS5cIJAKJQN8IwEHRsGImWSwyM1g407kRNsYNp58IJAKJwKoQgK+iQUX4cte+WPmPb5SbbgVYL10ikAgkAqtGQG6yHyyBbS32R2vMAuknAolAIjB0BCC2XVpc7OQnwfjQO5/9SwQSgc1FoOapstgPgfHOEuti6RKBRCARGAMCGl6QWiEyNsCSCJHVTDeGmrDdIgAAAIBJREFUC8o+JgKJwOYhIJFx5buee+65Ql5MLesnlZsHTV5xIpAIjAEBDC6JDP+yDz/6ZOuTr6z+k5guEUgEEoEhIlATmH3cdf78+fL2OF+54C3ynFoKTfqJQCIwNAQksrpfuy5sJJt8fcES+xb7xDDX6hIZTwQSgURgIAhMmzH+P88Yb+f/p9ThAAAAAElFTkSuQmCC";
			}
			$this -> request -> data['social_login_type'] = $this -> request -> data['socialType'];
			$user = $this -> User -> findByUsername($this -> request -> data['username']);
			if (isset($user['User']['id'])) {
				$this -> request -> data['id'] = $user['User']['id'];
				$this -> request -> data['password'] = $user['User']['password'];
			} else {
				$this -> request -> data['password'] = uniqid();
			}
			if(strpos($this -> request -> data['featured_image'], 'http') !== false){
				unset($this -> request -> data['featured_image']);
			}
			$this -> User -> save($this -> request -> data);
			$this -> request -> data['socialType'] = null;
			$this -> login();
		}
	}

	public function getImageBytes($imageURL) {
		return "data:image/png;base64," . base64_encode(file_get_contents($imageURL));
	}

	public function passwordReset() {
		$this -> setAsJSON();
		$error = "Unknown email address!";
		$data = $this -> request -> data;
		$user = $this -> User -> find('first', array('conditions' => array('User.email' => $data['email'])));
		if ($user) {
			$user['User']['password'] = uniqid();
			$this -> User -> save($user['User']);
			$this -> jsonResponse(array('success' => true));
			$Email = new CakeEmail();
			$Email -> template('reset_password', 'default');
			$Email -> emailFormat('html');
			$Email -> to($data['email']);
			$Email -> subject('Can I Kick it password reset');
			$Email -> from('jjohnson.java@gmail.com');
			$Email -> viewVars(array('user' => $user['User']));
			$Email -> send();
		} else {
			$this -> jsonResponse(array('success' => false, 'error' => $error));
		}
	}

	public function logout() {
		$this -> setAsJSON();
		CakeSession::write('user', null);
		$this -> jsonResponse(array('success' => true));
	}

	public function addChallangesToUser() {
		$this -> setAsJSON();
		$ids = json_decode($this -> request -> data['ids']);
		$userId = $this -> request -> data['dataId'];
		$this -> ChallangesUsers -> deleteAll(array('ChallangesUsers.user_id' => $userId), false);
		$many = array();
		for ($i = 0; $i < sizeof($ids); $i++) {
			array_push($many, array('challange_id' => $ids[$i], 'user_id' => $userId));
		}
		$this -> ChallangesUsers -> saveMany($many);
		$this -> jsonResponse(array('success' => true));
	}

}
