<?php

App::uses('RestController', 'Controller');

class FeaturedImageController extends RestController {
	public $uses = array('User', 'Advertisment', 'Special', 'Trip');
	public $layout = 'ajax';
	
	public function get($model, $id) {
		$this -> autoRender = false;
		$tableName = Inflector::underscore(Inflector::pluralize($model));
		$results = $this->$model->query("SELECT featured_image FROM $tableName WHERE id=$id;");
		$imgBytes = $results[0][$tableName]['featured_image'];

		if(strpos($imgBytes, "png") !== false){
			$this -> response -> type('image/png');
		}else{
			$this -> response -> type('image/jpeg');
		}
		$this -> response -> body($this->decode($imgBytes));
	}
	
	public function uploadStatic(){
		$this -> autoRender = false;
		$bytes = $this->request->data['fileBytes'];
		if(strpos($bytes, "png") !== false){
			$ext = '.png';
		}else{
			$ext = '.jpg';
		}
		$this -> response -> type('text');
		$fileName = uniqid() . $ext;
		$location = WWW_ROOT . '/img/static_upload/' . $fileName;
		file_put_contents($location, $this->decode($bytes));
		$this -> response -> body($fileName);
	}
}
