<?php

App::uses('RestController', 'Controller');

class TipsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Tip');
	
	public function index() {
		$this -> returnItems($this -> Tip, 'tips');
	}

	public function view($id) {
		$tip = $this -> Tip -> findById($id);
		$this -> set(array('promotion' => $tip, '_serialize' => array('promotion')));
	}
	
	public function add() {
		if ($this -> Tip -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}
	

	public function edit($id) {
		$this -> Tip -> id = $id;
		if ($this -> Tip -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Tip -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}
	
}
