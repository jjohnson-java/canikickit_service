<?php


/*
 * Task Comment: Facebook: (need email addresses to make admins) https://business.facebook.com/pages/Can-I-Kick-It/801832009885215

Twitter: (CanIKickItApp / pw xcite123) http://twitter.com/canikickitapp

G : (need email addresses to make admins) https://plus.google.com/b/107383439182976978201

Pinterest: (ben@xcitemediagroup.com / xcite123) http://www.pinterest.com/canikickitapp/

instagram: (ben@xcitemediagroup.com / xcite123) http://instagram.com/canikickitapp
 */

App::uses('RestController', 'Controller');

define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/lib/Facebook/');

require __DIR__ . '/lib/Twitter/tmhOAuth.php';
require __DIR__ . '/lib/Facebook/autoload.php';
require __DIR__ . '/lib/Instagram/Instagram.php';
require __DIR__ . '/lib/Google/google-api-php-client-master/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

class SocialMediaController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('SocialSharing', 'SystemVariable', 'User');

	private function saveImage($imageBytes){
		$filename = __DIR__ . '/social_images/' . uniqid() . '.png';
		file_put_contents($filename, base64_decode($imageBytes));
		return $filename;
	}
	
	public function postToFaceBook() {
		$user = $this->getUser();
		$this -> setAsJSON();
		$title = $this -> request -> data['title'];
		$message = $this -> request -> data['message'];
		FacebookSession::setDefaultApplication($this->config('facebook_api_id'), $this->config('facebook_app_secert'));
		$session = new FacebookSession($this->request->data['token']);
		try {
			$req = new FacebookRequest($session, 'POST', '/801832009885215/feed', array('message' => $message));
			$req -> execute() -> getGraphObject();
		} catch (FacebookRequestException $ex) {
		   $this -> jsonResponse(array('success' => false, 'message' => print_r($ex, true)));
		   return;
		} catch (\Exception $ex) {
		  	$this -> jsonResponse(array('success' => false, 'message' => print_r($ex, true)));
		 	return;
		} catch(FacebookRequestException $e) {
			$this -> jsonResponse(array('success' => false, 'message' => print_r($e, true)));
			return;
		}
		$this -> jsonResponse(array('success' => true));
	}
	
	private function config($key){
		return $this->SystemVariable->geValueByKey($key);
	}

	public function postToTwitter() {
		$user = $this->getUser();
		$this -> setAsJSON();
		$title = $this -> request -> data['title'];
		$message = $this -> request -> data['message'];
		$token = $this->request->data['token'];
		$tmhOAuth = new tmhOAuth(array(
				'consumer_key' => $this->config('twitter_consumer_key'), 
				'consumer_secret' => $this->config('twitter_consumer_secert'), 
				'token' => $token, 
				'secret' => $this->config('twitter_oauth_token_secert'),
				'bearer' => $this->config('twitter_oauth_token')
			)
		);
		$tmhOAuth -> request('POST', 'https://upload.twitter.com/1.1/statuses/update.json', array('status' => $message ), true, true);
		$this -> jsonResponse(array('success' => true));
	}

	public function postToGoogle() {
		$user = $this->getUser();
		$this -> setAsJSON();
		$title = $this -> request -> data['title'];
		$message = $this -> request -> data['message'];
		$token = $this->request->data['token'];
		$client = new Google_Client();
		$client -> setApplicationName($this->config('google_application_name'));
		$client -> setDeveloperKey($this->config('google_api_id'));
		$ticket = $client->verifyIdToken($token);
		$data = $ticket->getAttributes();
		$userId = $data['payload']['sub'];
		$plus = new Google_Service_Plus($client);
		$moment_body = new Google_Moment();
		$moment_body->setType("http://schema.org/AddAction");
		$item_scope = new Google_ItemScope();
		$item_scope->setId(uniqid());
		$item_scope->setType("http://schema.org/AddAction");
		$item_scope->setName($title);
		$item_scope->setDescription($message);
		$moment_body->setObject($item_scope);
		$momentResult = $plus->moments->insert('me', 'vault', $moment_body);
		$this -> jsonResponse(array('success' => true));
	}

	public function postToInstagram() {
		$user = $this->getUser();
		$this -> setAsJSON();
		$this -> request -> data['user_id'] = $user['User']['id'];
		$title = $this -> request -> data['title'];
		$message = $this -> request -> data['message'];
		$token = $this->request->data['token'];
		$instagram = new Instagram($this->config('instagram_app_key'));
		$instagram->setAccessToken($this->request->data['token']);
		$result = $instagram->likeMedia('http://canikickitapp.com');
	}

}
