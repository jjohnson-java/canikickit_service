<?php

App::uses('RestController', 'Controller');

class ProxyPostController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('');
	
	public function post() {
		$this -> setAsJSON();
		$options = json_decode($this->request->query['options'], true);
		
		$url = $options['url'];
		
		$data = $options['data'];
		$header = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data),
		    ),
		);
		$context  = stream_context_create($header);
		$result = file_get_contents($url, false, $context);
		
		echo json_encode($result);
	}

}
