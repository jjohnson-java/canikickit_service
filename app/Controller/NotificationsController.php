<?php

App::uses('RestController', 'Controller');

class NotificationsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Notification');
	
	public function index() {
		$this -> returnItems($this -> Notification, 'notifications');
	}

	public function view($id) {
		$notification = $this -> Notification -> findById($id);
		$this -> set(array('notification' => $notification, '_serialize' => array('notification')));
	}
	
	
	
	
	public function add() {
		
		$ERROR_EMAIL = "jjohnson.java@gmail.com";
		$URL = "https://api.parse.com/1/push";
	
		function push($app, $URL){
			try{
				$restAPIKey = $app['restkey'];
				$appId = $app['appid'];
				$message = $app['message'];
				$data = array(
					'where'=> array(
			          "deviceType" => $deviceType == null ? 'ios' : $deviceType
			       	),
				    'data' => array(
				        'alert' => $message
				    )
				);
				$_data = json_encode($data);
				echo "DATA SENT: <br/>" . $_data . "<br/><br/>";
				
				$headers = array(
				    'X-Parse-Application-Id: ' . $appId,
				    'X-Parse-REST-API-Key: ' . $restAPIKey,
				    'Content-Type: application/json',
				    'Content-Length: ' . strlen($_data),
				);
				echo "RESPONSE: ";
				
				$curl = curl_init($URL);
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
				curl_exec($curl);
				
				echo "<br/><br/>";
				
			}catch(Exception $e){
				error_log($e->message, 1, $ERROR_EMAIL);
			}
		}
		
		if ($this -> Notification -> save($this->handleActive($this -> request -> data))) {
			push(array(
				"name"=>"Can I Kick it",
				"appid"=>"3CXtoEDGKVvLG9w1ZPj7xvITo91zOoaGRL4OOl4n",
				"restkey"=>"JeaIhFCpcCdwnxC8gYqjN17t5g4kpFHGivutQ7zQ",
				"deviceTypes"=>array("ios","android"),
				"message"=>$this -> request -> data['message']
			), $URL);
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> Notification -> id = $id;
		if ($this -> Notification -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Notification -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}
	
	public function getByUserId($userId){
		$this -> setAsJSON();
		$notifications = $this -> UsersNotifications -> find('all', array('conditions' => array('UsersNotifications.user_id' => $userId)));
		$this -> jsonResponse(array('notifications' => $notifications));
	}
	
	public function getUserNotification($id){
		$this -> setAsJSON();
		$notifications = $this -> UsersNotifications -> findById($id);
		$this -> jsonResponse(array('notification' => $notifications));
	}
	
}
