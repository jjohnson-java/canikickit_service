<?php

App::uses('RestController', 'Controller');

App::uses('CakeEmail', 'Network/Email');

class SpecialsController extends RestController {

	public $components = array('RequestHandler');

	public $uses = array('User', 'PurchasedSpecial', 'SystemVariable', 'SpecialsUsers', 'Special', 'SpecialsRewards');

	private function intValue($s) {

		if (!isset($s) || $s == null || $s != "") {

			$s = intval($s);

			if (!is_int($s)) {

				$s = 0;

			}

		} else {

			$s = 0;

		}

		return $s;

	}

	public function submitWebsiteSpecial() {

		$this -> setAsJSON();

		$systemVar = $this -> SystemVariable -> findByKey('website_submission_code');

		$thePromoCode = $systemVar['SystemVariable']['value'];

		$email = $this -> request -> data['email'];

		$promoCode = $this -> request -> data['promo_code'];

		$name = $this -> request -> data['name'];

		$message = "Success! " . $name . ", you have successfully been entered into the sweepstakes. Good Luck!";

		$success = true;

		if ($email == "" || $promoCode == "" || $name == "") {

			$message = "All fields are required.";

			$success = false;

		} else if ($promoCode != $thePromoCode) {

			$message = "Incorect promo code.";

			$success = false;

		} else {

			$user = $this -> User -> findByEmail($email);

			if (!isset($user['User'])) {

				$names = explode(" ", $name);

				$user = $this -> User -> save(array("username" => $email, "email" => $email, "first_name" => $names[0], "last_name" => $names[1], "password" => uniqid()));

			}

			$specialUser = $this -> SpecialsUsers -> findByUserId($user['User']['id']);

			if (isset($specialUser['SpecialsUsers'])) {

				$message = "You have already been entered into the sweepstakes. Good Luck!";

				$success = false;

			} else {

				$this -> SpecialsUsers -> save(array("user_id" => $user['User']['id'], "special_id" => "website"));

				$Email = new CakeEmail();

				$Email -> template('sweepstakes_entry', 'default');

				$Email -> emailFormat('html');

				$Email -> to($email);

				$Email -> subject('Can I Kick it Sweepstakes Entry');

				$Email -> from('scott@canikickitco.com');

				$Email -> viewVars(array('name' => $name));

				$Email -> send();

			}

		}

		$this -> jsonResponse(array('success' => $success, 'message' => $message));

	}

	public function purchaseSpecial($id) {

		$this -> setAsJSON();

		$allSpecials = $this -> PurchasedSpecial -> findAllByUserIdAndContributorId($this -> request -> data['userId'], $this -> request -> data['contributor_id']);

		$newBar = sizeof($allSpecials) == 0;

		$this -> PurchasedSpecial -> save(array('user_id' => $this -> request -> data['userId'], 'contributor_id' => $this -> request -> data['contributor_id'], 'special_id' => $id));

		$user = $this -> User -> findById($this -> request -> data['userId']);

		$user['User']['isFirstSpecialSubmit'] = $user['User']['number_of_tickets'] == 0;

		$user['User']['number_of_tickets'] = $this -> intValue($user['User']['number_of_tickets']) + 5;

		$user['User']['number_of_bars_attended'] = $newBar ? $this -> intValue($user['User']['number_of_bars_attended']) + 1 : $this -> intValue($user['User']['number_of_bars_attended']);

		unset($user['User']['featured_image']);

		$user = $this -> User -> save($user['User']);

		$this -> jsonResponse(array('success' => true, 'user' => $user));

	}

	public function complete($specialId, $userId, $contributorId, $promotionId) {

		$this -> SpecialsComplete -> save(array("special_id" => $specialId, "user_id" => $userId, "contributor_id" => $contributorId, 'promotion_id' => $promotionId));

		$data = $this -> Promotion = getCompleteionData($this, $promotionId, $userId);

		if ($data['num_contributors_complete'] == $data['total_contributors']) {

			$this -> PromotionsComplete -> save(array("promotion_id" => $promotionId, "user_id" => $userId));

		}

	}

	public function index() {

		$this -> returnItems($this -> Special, 'specials', null, array("user_id" => $this -> request -> query['dataId']));

	}

	public function view($id) {

		$special = $this -> Special -> findById($id);

		$this -> set(array('special' => $special, '_serialize' => array('special')));

	}

	public function add() {

		if ($this -> Special -> save($this -> handleActive($this -> request -> data))) {

			$message = 'Saved';

		} else {

			$message = 'Error';

		}

		$this -> set(array('message' => $message, '_serialize' => array('message')));

	}

	public function edit($id) {

		$this -> Special -> id = $id;

		if ($this -> Special -> save($this -> handleActive($this -> request -> data))) {

			$message = 'Saved';

		} else {

			$message = 'Error';

		}

		$this -> set(array('message' => $message, '_serialize' => array('message')));

	}

	public function delete($id) {

		if ($this -> Special -> delete($id)) {

			$message = 'Deleted';

		} else {

			$message = 'Error';

		}

		$this -> set(array('message' => $message, '_serialize' => array('message')));

	}

	public function getByPromtoionId($id) {

		$loggedInUser = CakeSession::read('user');

		$this -> setAsJSON();

		$pUsers = $this -> PromotionsUsers -> find('all', array('conditions' => array('PromotionsUsers.promotion_id' => $id)));

		$promotion = $this -> Promotion -> findById($id);

		$return = array();

		foreach ($pUsers as $pUser) {

			$user = $this -> User -> findById($pUser['PromotionsUsers']['user_id']);

			$specialsUsers = $this -> SpecialsUsers -> findByUserId($user['User']['id']);

			foreach ($specialsUsers as $specialsUser) {

				$special = $this -> Challange -> findById($specialsUser['special_id']);

				$special['Challange']['Contribtor'] = $user;

				$special['Challange']['SpecialsComplete'] = $this -> SpecialsComplete -> findExact($special['Challange']['id'], $user['User']['id'], $loggedInUser['User']['id']);

				$special['Promotion'] = $promotion;

				array_push($return, $special);

			}

		}

		$this -> jsonResponse(array('specials' => $return));

	}

	public function addRewardsToChallange() {

		$this -> setAsJSON();

		$ids = json_decode($this -> request -> data['ids']);

		$specialId = $this -> request -> data['dataId'];

		$this -> SpecialsRewards -> deleteAll(array('SpecialsRewards.special_id' => $specialId), false);

		$many = array();

		for ($i = 0; $i < sizeof($ids); $i++) {

			array_push($many, array('special_id' => $specialId, 'reward_id' => $ids[$i]));

		}

		$this -> SpecialsRewards -> saveMany($many);

		$this -> jsonResponse(array('success', true));

	}

}
