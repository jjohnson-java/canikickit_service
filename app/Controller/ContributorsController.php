<?php

App::uses('RestController', 'Controller');

class ContributorsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('User', 'PurchasedSpecial');

	public function index() {
		$this -> returnItems($this -> User, 'consumers', null, array('type' => 'contributor'));
	}

	public function export(){
		$this->exportCsv($this->User->find('all', array('type' => 'contributor')));
	}

	public function activeContributors() {
		$this -> returnItems($this -> User, 'consumers', null, array('type' => 'contributor'));
	}

	public function getPlace($id) {
		$this -> setAsJSON();
		$url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$id&key=AIzaSyBhi4JKfn0yMm-Yzm3vjy0FECW2kIMEyH8";
		$d = json_decode(file_get_contents($url), true);

		$this -> jsonResponse($d['result']);
	}
	
	public function overview() {
		$this -> setAsJSON();
		$user = $this->getUser();
		$return = array();
		$allContributors = $this->User->findAllByType('contributor');

		foreach($allContributors as $contributor){
			$cid = $contributor['User']['id'];
			$uid = $user['User']['id'];
			$contributor['User']['PurchasedSpecial'] = $this->PurchasedSpecial->findAllByUserIdAndContributorId($uid, $cid);
			array_push($return, $contributor);
		}

		$this -> jsonResponse(array('contributors' => $return));
	}

	public function allOfThem() {
		$this -> setAsJSON();
		$user = $this->getUser();
		$return = array();
		$allContributors = $this->User->findAllByType('contributor');
		$contributors = $this->User->findAllByTypeAndShowinUi('contributor', 1);
		$all = $this->User->findAllByTypeAndShowinUi('contributor', 0);

		foreach($contributors as $contributor){
			$cid = $contributor['User']['id'];
			$uid = $user['User']['id'];
			$contributor['User']['PurchasedSpecial'] = $this->PurchasedSpecial->findAllByUserIdAndContributorId($uid, $cid);
			array_push($return, $contributor);
		}

		$this -> jsonResponse(array('contributors' => $return, 'allContributors' => $allContributors, 'all' => $all));
	}

	private function getContributors(){
		$user = $this->getUser();
		$contributors = $this -> User -> find('all', array('conditions' => array('type' => 'contributor')));
		$return = array();
		foreach($contributors as $contributor){
			$cid = $contributor['User']['id'];
			$uid = $user['User']['id'];
			$contributor['User']['PurchasedSpecial'] = $this->PurchasedSpecial->findAllByUserIdAndContributorId($uid, $cid);
			array_push($return, $contributor);
		}
		return $return;
	}

	public function view($id) {
		$user = $this -> User -> findById($id);
		$this -> set(array('user' => $user, '_serialize' => array('user')));
	}

	public function add() {
		$this -> setAsJSON();
		$message = null;
		if ($this -> isUserNameUnique($this -> request -> data['username'])) {
			$data = $this -> handleActive($this -> request -> data);
			$data['type'] = 'contributor';
			$success = $this -> User -> save($data);
			if ($success) {
				$success = true;
			} else {
				$success = false;
			}
		} else {
			$message = 'Username not unique';
			$success = false;
		}

		$this -> jsonResponse(array('success' => $success, 'errorMessage' => $message));
	}

	public function edit($id) {
		if (isset($this -> request -> form[0]['tmp_name'])) {
      $fileContents = base64_encode(file_get_contents($this -> request -> form[0]['tmp_name']));
      $filename = 'upload_bytes_' . uniqid();
  		file_put_contents(WWW_ROOT . 'files/' . $filename, $fileContents);
      $this -> request -> data['menu'] = $filename;
    }
		$this -> setAsJSON();
		$this -> User -> id = $id;
		if ($this -> User -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> jsonResponse(array('message' => $message));
	}

	public function delete($id) {
		if ($this -> User -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
