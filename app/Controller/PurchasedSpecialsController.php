<?php

App::uses('RestController', 'Controller');

class PurchasedSpecialsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('PurchasedSpecial');

	public function index() {
		$this -> returnItems($this -> PurchasedSpecial, 'purchased_specials');
	}

	public function view($id) {
		$purchasedSpecial = $this -> PurchasedSpecial -> findById($id);
		$this -> set(array('purchased_special' => $purchasedSpecial, '_serialize' => array('purchased_special')));
	}

	public function export(){
		$this->exportCsv($this->PurchasedSpecial->find('all'));
	}

}
