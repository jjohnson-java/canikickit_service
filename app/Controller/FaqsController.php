<?php

App::uses('RestController', 'Controller');

class FaqsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Faq');

	public function index() {
		$this -> returnItems($this -> Faq, 'faqs');
	}

	public function export(){
		$this->exportCsv($this->Faq->find('all'));
	}

	public function view($id) {
		$this->setAsJSON();
		$faq = $this -> Faq -> findById($id);
		$this -> set(array('faq' => $faq, '_serialize' => array('faq')));
		$this -> jsonResponse($faq);
	}

	public function add() {
		if ($this -> Faq -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> Faq -> id = $id;
		if ($this -> Faq -> save($this -> request -> data)) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Faq -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
