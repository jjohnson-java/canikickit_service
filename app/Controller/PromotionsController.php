<?php

App::uses('RestController', 'Controller');

class PromotionsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Promotion');

	private function intValue($s) {
		if (!isset($s) || $s == null || $s != "") {
			$s = intval($s);
			if (!is_int($s)) {
				$s = 0;
			}
		} else {
			$s = 0;
		}
		return $s;
	}
	
	public function index() {
		$this -> returnItems($this -> Promotion, 'Promotions', null, array("sweepstake_id" => $this->request->query['dataId']));
	}

	public function view($id) {
		$Promotion = $this -> Promotion -> findById($id);
		$this -> set(array('Promotion' => $Promotion, '_serialize' => array('Promotion')));
	}

	public function add() {
		if ($this -> Promotion -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> Promotion -> id = $id;
		if ($this -> Promotion -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Promotion -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}


}
