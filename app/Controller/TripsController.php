<?php

App::uses('RestController', 'Controller');

class TripsController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Trip', 'UsersTrips');

	public function index() {
		$this -> returnItems($this -> Trip, 'trips');
	}

	public function view($id) {
		$trip = $this -> Trip -> findById($id);
		$this -> set(array('trip' => $trip, '_serialize' => array('trip')));
	}

	public function export(){
		$this->exportCsv($this->Trip->find('all'));
	}

	public function add() {
		if ($this -> Trip -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function edit($id) {
		$this -> Trip -> id = $id;
		if ($this -> Trip -> save($this->handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function delete($id) {
		if ($this -> Trip -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

	public function getByUserId($userId){
		$this -> setAsJSON();
		$trips = $this -> UsersTrips -> find('all', array('conditions' => array('UsersTrips.user_id' => $userId)));
		$this -> jsonResponse(array('trips' => $trips));
	}

	public function getUserTrip($id){
		$this -> setAsJSON();
		$trips = $this -> UsersTrips -> findById($id);
		$this -> jsonResponse(array('trip' => $trips));
	}

}
