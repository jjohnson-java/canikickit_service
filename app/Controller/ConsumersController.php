<?php
require_once dirname(__FILE__) . "/lib/simpletest/browser.php";
require_once dirname(__FILE__) . "/lib/simple_html_dom.php";

App::uses('RestController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ConsumersController extends RestController {
	public $components = array('RequestHandler');
	public $uses = array('Promotion', 'Trip', 'User', 'SystemVariable', 'SpecialsUsers', 'Sweepstake');

	public function index() {
		$this -> returnItems($this->User, 'consumers',  null, array('type' => 'consumer'));
	}

	public function export(){
		$this->exportCsv($this->User->find('all', array('type' => 'consumer')));
	}

	public function view($id) {
		$user = $this -> User -> findById($id);
		$this -> set(array('user' => $user, '_serialize' => array('user')));
	}

	public function add() {
		$this -> setAsJSON();
		if ($this -> isUserNameUnique($this -> request -> data['username'])) {
			$data = $this -> handleActive($this -> request -> data);
			$success = $this -> User -> save($data);
			if ($success) {
				$Email = new CakeEmail();
				$Email -> template('welcome', 'default');
				$Email -> emailFormat('html');
				$Email -> to($data['email']);
				$Email -> subject('Can I Kick it registration');
				$Email -> from('jjohnson.java@gmail.com');
				$Email -> viewVars(array('user' => $data));
				$Email -> send();
				$message = 'Saved';
			} else {
				$message = 'Error';
			}
		} else {
			$message = 'Username not unique';
		}

		$this -> jsonResponse(array('success' => $success, 'message' => $message));
	}

	public function edit($id) {
		if (isset($this -> request -> form[0]['tmp_name'])) {
			$fileContents = base64_encode(file_get_contents($this -> request -> form[0]['tmp_name']));
			$this -> request -> data['menu'] = $fileContents;
		}
		$this -> setAsJSON();
		$this -> User -> id = $id;
		if ($this -> User -> save($this -> handleActive($this -> request -> data))) {
			$message = 'Saved';
		} else {
			$message = 'Error';
		}
		$this -> jsonResponse(array('message' => $message));
	}

	public function delete($id) {
		if ($this -> User -> delete($id)) {
			$message = 'Deleted';
		} else {
			$message = 'Error';
		}
		$this -> set(array('message' => $message, '_serialize' => array('message')));
	}

}
