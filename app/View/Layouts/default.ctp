<!DOCTYPE html>
<html>
	<head>
		<?php echo $this -> Html -> charset(); ?>
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<link rel="icon" href="<?php echo $this -> Html -> url('/', true); ?>img/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="<?php echo $this -> Html -> url('/', true); ?>img/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="<?php echo $this -> Html -> url('/', true); ?>img/icon.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this -> Html -> url('/', true); ?>img/icon.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this -> Html -> url('/', true); ?>img/icon.png" />
		<link rel="apple-touch-startup-image" href="<?php echo $this -> Html -> url('/', true); ?>img/splash.png" />
		<link rel="apple-touch-startup-image" sizes="768x1004" href="<?php echo $this -> Html -> url('/', true); ?>img/splash.png" />
		<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alfa+Slab+One|Roboto:400,700,300">
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="<?php echo $this -> Html -> url("/", true); ?>js/pace.min.js"></script>
		<script src="<?php echo $this -> Html -> url("/", true); ?>js/phone-format.js"></script>
		<script src="<?php echo $this -> Html -> url("/", true); ?>js/jquery-te-1.4.0.min.js"></script>
		<?php
		echo $this -> Html -> meta('icon');

		$this -> Combinator -> add_libs('css', array('pace', 'featherlight', 'fetherlightoverrides', 'jquery.admin', 'jquery.alerts', 'jquery.datetimepicker', 'jquery.mask', 'jquery.verticaltabs', 'pure-min', 'jquery.sortable', 'tooltipster', 'jquery-te-1.4.0', 'bootstrap', 'bootstrap-theme', 'jquery.bootgrid', 'app_labels_editor', 'system_init'));
		$this -> Combinator -> add_libs('js', array('system_init', 'featherlight', 'jsoneditor', 'jquery.labelsEditor', 'jquery.bootgrid', 'jquery.printElement', 'qrcode', 'jquery.multiselect.dialog', 'jquery.tooltipster', 'jquery.total-storage', 'jquery.ajax.form', 'jquery.ajax.form.submit', 'jquery.ajax.proxy', 'jquery.ajax.select', 'jquery.maintence.grid', 'jquery.dashboard-chart', 'jquery.cookie', 'jquery.mjs.nestedSortable', 'jquery.enterbind', 'jquery.mask', 'jquery.maskedinput', 'jquery.alerts', 'jquery.verticaltabs', 'jquery.datetimepicker', 'jquery.ajax.select', 'ejs', 'jquery.admin', 'jquery.functions'));
		
		echo $this -> Combinator -> scripts('css');
		echo $this -> Combinator -> scripts('js');

		echo $this -> fetch('meta');
		echo $this -> fetch('css');
		echo $this -> fetch('script');
		?>
		<script type="text/javascript">var BASE_FULL_URL =   "<?php echo $this -> Html -> url('/', true); ?>";
		</script>
	</head>
	<body>
		<div class='flash-content-hidden'><?php echo $this -> Session -> flash(); ?></div>
		<?php echo $this -> fetch('content'); ?>
	</body>
</html>
